var albumCtrl = {
	
	init: function() {
		this.initItems();
	},
	
	initItems: function() {	
		$( ".add-btn" ).on( "click", function() {
			albumCtrl.addRow();
		});
		
		$( ".table-collection" ).on( "click", "a.delete-btn", function() {
			if ( confirm( "Delete record?" ) ) {
				albumCtrl.deleteRow( $( this ) );
			}
			return false;
	    });
	},
	
	addRow: function() {
		var itemPrototype = $( ".table-collection" ).attr( "data-prototype" );
		var current = $( ".table-collection tr:last" ).data( "key" );
		var rowCount = 0;
		if ( !isNaN( current ) ) {
			rowCount = current + 1;
		}
    	var $newRow = $( $.trim( itemPrototype.replace( /__name__/g, rowCount ) ) );
    	$( ".table-collection" ).append( $newRow );
    	$newRow.attr( "data-key", rowCount );
    	
    	$( ".file-input" ).pixelFileInput({ placeholder: "No file selected..." });
	},
	    
	deleteRow: function( $btn ) {
		$btn.closest( "tr" ).remove();
	},
};

$( document ).ready( function() {
	albumCtrl.init();
});