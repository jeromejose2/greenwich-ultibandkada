var galleryControl = {
	page: 1,
	order: 1,
	albumId: 0,
	galleryRequest: null,
	albumRequest: null,
	lytebox: new Lytebox(),
	lyteboxHtml: null,
	lyteboxContainer: null,
	currentMedia: null,
	spinner: null,
	
	init: function() {
		$( "#img-template a.btn-close" ).click( function( e ) {
			galleryControl.lytebox.close();
			galleryControl.lyteboxContainer.html( "" );
			$( "div.media-paginator a.btn-page-left" ).addClass( "hide" );
			$( "div.media-paginator a.btn-page-right" ).addClass( "hide" );
			e.preventDefault();
		});
		
		$( "body" ).on( "change", "div.select-wrap select", function() {
			galleryControl.refreshList( galleryControl.page, $( this ).val() );
			galleryControl.order = $( this ).val();
		});
		
		$( "body" ).on( "click", "div.gallery-paginator a.btn-page-left, div.gallery-paginator a.btn-page-right", function( e ) {
			galleryControl.refreshList( $( this ).data( "page" ), galleryControl.order );
			galleryControl.page = $( this ).data( "page" );
			e.preventDefault();
		});
		
		$( "body" ).on( "click", ".list-wrapper > li > a", function( e ) {
			scrollTo( "<a href='#gallery'>" );
			
			galleryControl.refreshAlbum( $( this ).data( "album-id" ) );
			galleryControl.albumId = $( this ).data( "album-id" );
			
			$( ".gallery-inner" ).removeClass( "animateOut" );
			$( ".gallery-inner" ).addClass( "animateIn");
			e.preventDefault();
		});
		
		$( "body" ).on( "click", ".btn-backtoalbum", function( e ) {
			$( ".gallery-inner" ).removeClass( "animateIn" );
			$( ".gallery-inner" ).addClass( "animateOut");
			$( ".gallery-inner div.icontent" ).html( "" );
		});
		
		$( "body" ).on( "click", "div.inner-pagination a.btn-page-left", function( e ) {
			$prev = $( ".imglist" ).not( ".hide" ).addClass( "hide" ).prev().removeClass( "hide" );
			galleryControl.toggleMediaListPaginator( $( "div.inner-pagination a.btn-page-left" ), $( "div.inner-pagination a.btn-page-right" ), $prev )
			e.preventDefault();
		});
		
		$( "body" ).on( "click", "div.inner-pagination a.btn-page-right", function( e ) {
			$next = $( ".imglist" ).not( ".hide" ).addClass( "hide" ).next( "ul" ).removeClass( "hide" );
			galleryControl.toggleMediaListPaginator( $( "div.inner-pagination a.btn-page-left" ), $( "div.inner-pagination a.btn-page-right" ), $next )
			e.preventDefault();
		});
		
		$( "body" ).on( "click", ".imglist > .item-img", function( e ) {
			galleryControl.currentMedia = $( this );
			galleryControl.loadLyteboxContent( galleryControl.currentMedia );
			
			galleryControl.lytebox.show( galleryControl.lyteboxHtml, { bgClose : false } );
			e.preventDefault();
		});
		
		$( "body" ).on( "click", "div.media-paginator a.btn-page-left", function ( e ) {
			if ( galleryControl.currentMedia.is( ":first-child" ) ) {
				galleryControl.currentMedia = galleryControl.currentMedia.parent().prev().children( "li:last" ); 
			} else {
				galleryControl.currentMedia = galleryControl.currentMedia.prev();
			}
			galleryControl.loadLyteboxContent( galleryControl.currentMedia );
			e.preventDefault();
		});
		
		$( "body" ).on( "click", "div.media-paginator a.btn-page-right", function ( e ) {
			if ( galleryControl.currentMedia.next().is( "div" ) ) {
				galleryControl.currentMedia = galleryControl.currentMedia.parent().next().children( "li:first" ); 
			} else {
				galleryControl.currentMedia = galleryControl.currentMedia.next();
			}
			galleryControl.loadLyteboxContent( galleryControl.currentMedia );
			
			e.preventDefault();
		});
	},
	
	refreshList: function( $page, $order ) {
		if ( galleryControl.galleryRequest ) {
			galleryControl.galleryRequest.abort();
			galleryControl.galleryRequest = null;
		}
		
		$( "div#gallery div.content" ).html( "" );
		$( "div#gallery .spinner" ).removeClass( "hide" ).show();
		
		galleryControl.galleryRequest = $.getJSON( app.base_uri + '/' + $page + '/' + $order + '/gallery', function( data ) {
			if ( data !== undefined && !$.isEmptyObject( data ) ) {
				$( "div#gallery .spinner" ).addClass( "hide" );
				$( "div#gallery div.content" ).html( data );
			}
		}).done( function() {
			galleryControl.galleryRequest = null;
		});
	},
	
	refreshAlbum: function( $id, callback ) {
		if ( galleryControl.albumRequest ) {
			galleryControl.albumRequest.abort();
			galleryControl.albumRequest = null;
		}
		
		$( ".gallery-inner .spinner" ).removeClass( "hide" ).show();
		
		galleryControl.albumRequest = $.getJSON( app.base_uri + '/' + $id  + '/album', function( data ) {
			if ( data !== undefined && !$.isEmptyObject( data ) ) {
				$( ".gallery-inner .spinner" ).addClass( "hide" );
				$( "div.gallery-inner div.icontent" ).html( data );
				shareCtrl.addAttributes( $( "div.gallery-inner div.icontent .album" ), "list" );
				shareCtrl.shortenUrl( $( "div.gallery-inner div.icontent" ) );
				shareCtrl.onLoadAlbum = false;
			}
		}).done( function() {
			galleryControl.albumRequest = null;
			if( callback ) {
				callback();
			}
		});
	},
	
	isFirst: function( $media ) {
		if ( $media.is( ":first-child" ) && $media.parent().prev().length == 0 ) {
			return true;
		}
		
		return false;
	},
	
	isLast: function( $media ) {
		if ( $media.next().is( "div" ) && $media.parent().next().length == 0 ) {
			return true;
		}
		
		return false;
	},
	
	loadLyteboxContent: function( $media ) {
		shareCtrl.addAttributes( $media, "lytebox" );
		
		$container = galleryControl.lyteboxContainer;
		$container.html( "" ).append( galleryControl.spinner );
		if( $media.data( "media-yt" ) == "" ) {
			$container.append( "<img src='" + $media.data( "media-webpath" ) +"' class='lytebox-img'/>" );
			$container.find( ".lytebox-img" ).addClass( "hide" );
		} else {
			$container.append( $media.data( "media-yt" ) );
			$container.find( "iframe" ).addClass( "hide" );
		}
		
		$( "div.lytebox-content div.content" ).find( ".lytebox-img, iframe" ).on( "load", function() {
			$( "div.lytebox-content .spinner" ).addClass( "hide" );
			$( this ).removeClass( "hide" );
		});

		$( "div.media-paginator a.btn-page-left" ).addClass( "hide" );
		$( "div.media-paginator a.btn-page-right" ).addClass( "hide" );
		if ( !galleryControl.isFirst( $media ) ) {
			$( "div.media-paginator a.btn-page-left" ).removeClass( "hide" );
		}
		
		if ( !galleryControl.isLast( $media ) ) {
			$( "div.media-paginator a.btn-page-right" ).removeClass( "hide" );
		}
		
		shareCtrl.shortenUrl( $( ".lytebox-content .social-wrap" ) );
		shareCtrl.onLoadMedia = false;
	},
	
	toggleMediaListPaginator: function( $leftBtn, $rightBtn, $currentPage ) {
		$leftBtn.addClass( "hide" );
		$rightBtn.addClass( "hide" );
		
		$parent = $currentPage.parent();
		if ( !$currentPage.is( ":first-child" ) ) {
			$leftBtn.removeClass( "hide" );
		}
		if ( !$currentPage.is( ":last-child" ) ) {
			$rightBtn.removeClass( "hide" );
		}
	},
	
	loadSharedPage: function( albumId, mediaId ) {
		var mediaCallback = function() {
			if (typeof mediaId !== "undefined") {
				galleryControl.currentMedia = $( "li.item-img[data-media-id='" + mediaId + "']" );
				galleryControl.loadLyteboxContent( galleryControl.currentMedia );
				
				galleryControl.lytebox.show( galleryControl.lyteboxHtml, { bgClose : false } );
			}
		}
		
		scrollTo( "<a href='#gallery'>" );
		galleryControl.refreshAlbum( albumId, mediaCallback );
		galleryControl.albumId = albumId;
		
		$( ".gallery-inner" ).removeClass( "animateOut" );
		$( ".gallery-inner" ).addClass( "animateIn");
	}
};

$(function() {
	galleryControl.init();
});