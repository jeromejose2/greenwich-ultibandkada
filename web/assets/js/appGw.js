function clickTo(btn, target)
{
	$(btn).click(function()
	{
		$(target).toggleClass("active");
	});
}

clickTo(".burger",".mainNav");
clickTo(".btn-control",".selection-list");

function selectTab( root ){
	$( root +" > li > a").click( function(e){
		e.preventDefault();
		var tab = $( this );

		$( root +" > li > a" ).removeClass("active");
		tab.addClass("active");

		//reset menu
		$( root ).toggleClass("active");

		if(root==".selection-list"){
			$(".txtHolder").text( tab.text() );
		}
		
		//update div.preview
		if(root == ".selection-list") {
			manageTab( tab.parent().index() );
		}
	});
}

selectTab(".selection-list");
selectTab(".mainNav");

///////////////////////////////
//
//	SLIMCROLL
//
///////////////////////////////
$(document).ready(function() {
  $('.inner-content > .container').slimScroll({
    position: 'right',
    height: '380px',
    /*railVisible: true,
    alwaysVisible: true,*/
    railOpacity: 0.3,
    size: '7px',
    color: '#e72c2e',
    alwaysVisible: true
  });

  $('.overlay-preloader').show();
  $('.overlay-preloader').width( window.innerWidth );
  $('.overlay-preloader').height( window.innerHeight );
});


///////////////////////////////
//
//	LYTEBOX
//
///////////////////////////////
var lytebox = new Lytebox();
$('.spanGreen').click(function() {
	lytebox.load({url: app.base_uri + "/terms", bgClose : false });
});


///////////////////////////////
//
//	WAYPOINT
//
///////////////////////////////


//splash - home
var padding = 0;
var section = "";
function scrollTo (id) {
    section = $(id).attr('href');
    targetPixel = $(section).offset().top;
    
  	if(section!="#splash")
    {
        targetPixel=targetPixel - padding;
    } else {
    	section = "#home";
        targetPixel = 0;
    }
    
    $('html, body').animate({scrollTop : targetPixel });
    window.location.hash = section.replace('#','!');
}

var mainNav_li_a = $(".mainNav > li > a");
	
	mainNav_li_a.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var arrowDown = $(".btn.explore");
	arrowDown.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var gallery = $(".btn-view-gallery");
	gallery.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var registration = $(".regStatus");
	registration.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

function changeContent( targetId ) {
	var div, idx;

	$(".preview > div").each(function(index) {
		div = $(this);
		idx = div.data("index");

		if(idx == targetId ) div.show();
		else				 div.hide();
	});
}

changeContent(0);//initial

function manageTab( index ){
	changeContent( index );
}

function submitForm() {
	document.getElementById( "form" ).submit();
}


/** DATEPICKER **/
var today = new Date();
$( ".datepicker" ).datepicker({
	startDate: new Date(today.getFullYear() - 35, today.getDate(), today.getMonth()),
	endDate: today
});
/////////////////////////////////////////////
//			P H A S E 3
/////////////////////////////////////////////
window.onload = function(){
	$('.overlay-preloader').hide();

	var forceScrollToGallery="<a href='#gallery'>";
	$('.list-wrapper > li > a').click( function(e){
		scrollTo(forceScrollToGallery);

		$('.gallery-inner').removeClass('animateOut');
		$('.gallery-inner').addClass('animateIn');
	});

	$('.btn-backtoalbum').click( function(e){
		$('.gallery-inner').removeClass('animateIn');
		$('.gallery-inner').addClass('animateOut');
	});

	var forceScrollToFinalist="<a href='#finalist'>";
	$('.list-wrapper2 .btn-view-profile').click( function(){
		
		//display finalist details
		var finalistId = $( this ).data( "id" );
		$( ".finalist-inner .spinner" ).removeClass( "hide" ).show();
		
		$.get( app.base_uri + "/" + finalistId + "/finalist", function( response ) {
			$( ".inner-portfolio-wrapper" ).html( response );
		}).done( function() {
			$( ".finalist-inner .spinner" ).addClass( "hide" );
			scrollTo(forceScrollToFinalist);
			
			$('.finalist-inner').removeClass('animateOut');
			$('.finalist-inner').addClass('animateIn');
			
			$('div.finalist-inner .btn-backto-lists').on( "click", function(e){
				var iframeObj = $('#finalist .finalist-inner .clipvideo iframe');
				var url = iframeObj.attr('src');
				iframeObj.attr('src', '');
				iframeObj.attr('src', url);
				$('.finalist-inner').removeClass('animateIn');
				$('.finalist-inner').addClass('animateOut');

				e.preventDefault();
			});
		});
		
		return false;
	});
	
	updateResize();
}

$('.imglist > .item-img > a').click(function() {
	lytebox.load({url:'popupEntries.php', bgClose : false });
});

window.onresize = updateResize();

function updateResize(){
	//IMPORTANT
	var wrapperH =$('.finalist-wrapper').height();
	var innerH =$('.finalist-inner').height();
	
	if(wrapperH < innerH){
		/*$('.finalist-wrapper').height( innerH );*/
	} 

	if(wrapperH > innerH) {
		/*$('.finalist-inner').height( wrapperH );*/
	}
}