var fileControl = {
	init: function() {
		$( ".file-input" ).pixelFileInput({ placeholder: "No file selected..." });
		
		$( ".image-thumbnail" ).on( "click", ".change-image-btn", function() {
			$container = $( this ).closest( ".image-container" );
			
			$container.find( ".image-thumbnail" ).fadeOut( "slow", function() {
				$container.find( ".file-input-container" ).removeClass( "hide" ).fadeIn( "slow" );
			});
			$container.find( ".change-image" ).val( 1 );
		});
	}	
};

$( document ).ready( function() {
	fileControl.init();
});