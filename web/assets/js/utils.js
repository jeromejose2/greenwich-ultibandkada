// This is a utility function to get query parameters
// Usage: $.QueryString['get_parameter']
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'));
})(jQuery);

(function($) {
	$( '#content-wrapper' ).on( 'submit', 'form', function() {
		window.onbeforeunload = null;
	    var btn = $( this ).find( "button.submit-button" );
	    btn.button('loading');
	    btn.prepend( '<i class="fa fa-spinner fa-spin"></i> ');
	    setTimeout( function () {
	    	btn.find( '.fa' ).hide();
	    	btn.button( "reset" );
	    }, 99999);
	});
})(jQuery);

(function($) {
	$( ".select2" ).select2();
})(jQuery);

//gets arrayObject size
(function($) {
	$.arraySize = function(obj) {
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};
})(jQuery);

//format date MM/DD/YYYY
(function($) {
	$.formatDate = function(date) {
	    var dateObj = new Date(date);
	    return (dateObj.getMonth() + 1) + "/" + dateObj.getDate() + "/" + dateObj.getFullYear();
	};
})(jQuery);

//format null to ""
(function($) {
	$.formatNull = function(data) {
	    if (data == null) {
	    	return "";
	    } else {
	    	return data;
	    }
	};
})(jQuery);

//prompts user if user confirms leaving the page when form has dirty data
(function($) {
	$( "#content-wrapper" ).find( "form:not( .search-form )" ).find( "input, select, textarea" ).on( "change", function () {
		window.onbeforeunload = function() {
			return "You have not yet saved the changes you have made.";
		};
	});
})(jQuery);

//nl2br function
(function($) {
	$.nl2br = function(str, is_xhtml) {
		var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
		
		return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	}
})(jQuery);

//timeago
(function($) {
	$.timeago.settings.cutoff = 31557600000; //milliseconds
	$( ".timeago" ).timeago();
})(jQuery);