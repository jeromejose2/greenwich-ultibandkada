var shareCtrl = {
	shareUri: "",
	accessToken: "feb94ee1bbab31b0051524ad5684b7864b3aeba7",
	request: null,
	onLoadAlbum: false,
	onLoadMedia: false,
	
	init: function() {
		shareCtrl.shareFb();
		
		window.twttr = (function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
	},
	
	addAttributes: function( parent, type ) {
		var uri = shareCtrl.shareUri +  parent.attr( "data-share-uri" );
		var title = parent.data( "title" );
		var $container = type == "lytebox" ? $( ".lytebox-content" ) : parent.parent();
		
		$container.find( "a.btn-ifb, a.btn-itw" ).attr( "data-share-uri", uri );
		$container.find( "a.btn-itw" ).attr( "data-title", title );

		$container.find( "a.btn-itw" ).attr( "href", "https://twitter.com/share?url=" + encodeURIComponent( uri ) + "&text=" + encodeURIComponent( title ) );
	},
	
	shareFb: function() {
		$( "body" ).on( "click", "a.btn-ifb" ,function() {
			var uri = encodeURIComponent( $( this ).attr( "data-share-uri" ) );
			var popup = window.open("https://www.facebook.com/sharer/sharer.php?u=" + uri, "pop", "width=600, height=400, scrollbars=no");
			
			return false;
		});
	},
	
	shortenUrl: function( parent ) {
		if ( shareCtrl.request && shareCtrl.onLoadAlbum == false && shareCtrl.onloadMedia == false ) {
			shareCtrl.request.abort();
			shareCtrl.request = null;
		}
		
		var twitterBtn = parent.find( "a.btn-itw" );
		var uri = encodeURIComponent( twitterBtn.attr( "data-share-uri" ) );

		shareCtrl.request = $.ajax({
			url: "https://api-ssl.bitly.com/v3/shorten?access_token=" + shareCtrl.accessToken + "&longUrl=" + uri + "&format=txt",
			success: function( response ) {
				var title = encodeURIComponent( twitterBtn.attr( "data-title" ) );
				twitterBtn.attr( "href", "https://twitter.com/share?url=" + response + "&text=" + title );
				shareCtrl.request = null;
			}
		});
	}
};

$( document ).ready( function() {
	shareCtrl.init();
});