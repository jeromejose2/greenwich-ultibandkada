<section class="wsteps">
	<a href="javascript:void(0);" class="btn-close" onclick="lytebox.close();"></a>

	<ul class="steps-header">
		<li class="active">
			<span class="lbl-step">STEP1</span>
			<span class="lbl-sub">Input Code</span>
		</li>
		<li><span class="lbl-step">STEP2</span>
			<span class="lbl-sub">Register</span>
		</li>
		<li><span class="lbl-step">STEP3</span>
			<span class="lbl-sub">Vote</span>
		</li>
		<li>
			<span class="lbl-step">DONE</span>
		</li>
	<div class="clearfix"></div>
	</ul>

	<!--INPUT CODE-->
	<div class="usercode centered">
		<span class="lbl-title">INPUT CODE</span>
		
		<form>
			<input class="code-txt style-field" type="text" name="code">
		</form>

		<a href="javascript:void(0);" class="wbtn continue" data-frame="1"></a>
		<a href="javascript:void(0);" class="wbtn cancel"  onclick="lytebox.close();"></a>
	</div>

	<!--REGISTER-->
	<div class="wregistration d-none centered">
		<span class="lbl-title">REGISTER</span>

		<form>
			<span class="lbl-info">FIRST NAME AND LAST NAME</span>
			<input class="reg-txt style-field" type="text" name="name">

			<span class="lbl-info">EMAIL ADDRESS</span>
			<input class="reg-txt style-field" type="text" name="email">
			
			<span class="lbl-info">MOBILE NUMBER</span>
			<input class="reg-txt style-field" type="text" name="mobile">
			
			<span class="lbl-info">ADDRESS</span>
			<input class="reg-txt style-field" type="text" name="address">
		</form>

		<a href="javascript:void(0);" class="wbtn continue" data-frame="2"></a><br/>
		<a href="javascript:void(0);" class="wbtn back"     data-frame="2"></a>
	</div>

	<!--VOTES-->
	<div class="votes d-none centered">
		<div class="lbl-title">VOTE</div>

		<div id="comp-list">
			<!--item1-->
			<div class="item">
				<div class="img-entry">
					<div class="v-indicator active">
						<span>+</span><span class="vote-value">1</span>
					</div>
				</div>
				<div class="d-wrap">
					<div class="content">
						<span class="lbl-groupname"><!-- BANDARITAS TILL  -->ONE LINE</span>
						<a href="javascript:void(0);" class="btn-add-vote"></a>
						<a href="javascript:void(0);" class="btn-remove-vote active"></a>
					</div>
				</div>
			</div>

			<!--item2-->
			<div class="item">
				<div class="img-entry">
					<div class="v-indicator">
						<span>+</span><span class="vote-value">1</span>
					</div>
				</div>
				<div class="d-wrap">
					<div class="content">
						<span class="lbl-groupname"><!-- BANDARITAS TILL  -->ONE LINE WITH ANOTHER LINE</span>
						<a href="javascript:void(0);" class="btn-add-vote"></a>
						<a href="javascript:void(0);" class="btn-remove-vote"></a>
					</div>
				</div>
			</div>

			<!--item3-->
			<div class="item">
				<div class="img-entry">
					<div class="v-indicator">
						<span>+</span><span class="vote-value">1</span>
					</div>
				</div>
				<div class="d-wrap">
					<div class="content">
						<span class="lbl-groupname"><!-- BANDARITAS TILL  -->ONE LINE WITH ANOTHER LINE ANOTHER LINE</span>
						<a href="javascript:void(0);" class="btn-add-vote"></a>
						<a href="javascript:void(0);" class="btn-remove-vote"></a>
					</div>
				</div>
			</div>

			<!--item4-->
			<div class="item">
				<div class="img-entry">
					<div class="v-indicator active">
						<span>+</span><span class="vote-value">1</span>
					</div>
				</div>
				<div class="d-wrap">
					<div class="content">
						<span class="lbl-groupname"><!-- BANDARITAS TILL  -->ONE LINE WITH ANOTHER LINE</span>
						<a href="javascript:void(0);" class="btn-add-vote"></a>
						<a href="javascript:void(0);" class="btn-remove-vote active"></a>
					</div>
				</div>
			</div>

		</div>
		<br/>
		<a href="javascript:void(0);" class="wbtn continue" data-frame="3"></a><br/>
		<a href="javascript:void(0);" class="wbtn back"     data-frame="3"></a>
	</div>


	<!--THANK YOU -->
	<div class="w-ty d-none">
		<div class="lbl-title">THANK YOU!</div>
		<p class="lbl-desc">Your votes have been given to the band! Would you like to upload another coupon code?</p>
	
		<br>
		<a href="javascript:void(0);" class="wbtn yes" data-frame="4"></a>
		<a href="javascript:void(0);" class="wbtn no"  onclick="lytebox.close();"></a>
	</div>


</section>

<script type="text/javascript">

$(document).ready(function() {
  $("#comp-list").owlCarousel({
      autoPlay: false, //3000 Set AutoPlay to 3 seconds
      items : 3,
      navigationText: ["",""],
      navigation : true,
      itemsMobile:	[479,1],
      itemsTablet:	[1024,3]
  });
});

function onMobileBy(frame){
	var mq = window.matchMedia( "(max-width: 640px)" );
	if(mq.matches){
		if(frame==1){
			$('.wsteps .steps-header li:nth-child(1)').hide();
			$('.wsteps .steps-header li:nth-child(2)').show();
		}
		if(frame==2){
			$('.wsteps .steps-header li:nth-child(2)').hide();
			$('.wsteps .steps-header li:nth-child(3)').show();
		}
		if(frame==3){
			$('.wsteps .steps-header li:nth-child(3)').hide();
			$('.wsteps .steps-header li:nth-child(4)').show();
		}
		if(frame==4){
			$('.wsteps .steps-header li:nth-child(4)').hide();
			$('.wsteps .steps-header li:nth-child(1)').show();
		}

	} else {
		$('.wsteps > .steps-header > li').each(function(index){
			$(this).show();
		})
	}
}

$('.continue, .yes').click(function(){
	var _self = $(this);
	var frame = parseInt(_self.attr('data-frame'));

	//1
	if( frame == 1  ){
		$('.usercode').addClass('d-none');
		$('.wregistration').removeClass('d-none');

		$('.steps-header li:nth-child(2)').addClass('active');
	}
	//2
	else if( frame == 2  ){
		$('.wregistration').addClass('d-none');
		$('.votes').removeClass('d-none');

		$('.steps-header li:nth-child(3)').addClass('active');
	}
	//3
	else if( frame == 3  ){
		$('.votes').addClass('d-none');
		$('.w-ty').removeClass('d-none');

		$('.steps-header li:nth-child(4)').addClass('active');
	}
	//4
	else if( frame == 4  ){
		$('.w-ty').addClass('d-none');
		$('.usercode').removeClass('d-none');

		$('.steps-header li:nth-child(2)').removeClass('active');
		$('.steps-header li:nth-child(3)').removeClass('active');
		$('.steps-header li:nth-child(4)').removeClass('active');
	}

	onMobileBy(frame);
});
$('.back').click(function(){
	var _self = $(this);
	var frame = parseInt(_self.attr('data-frame'));

	//2
	if( frame == 2  ){
		$('.usercode').removeClass('d-none');
		$('.wregistration').addClass('d-none');

		$('.steps-header li:nth-child(2)').removeClass('active');
	}
	//3
	else if( frame==3 ){
		$('.votes').addClass('d-none');
		$('.wregistration').removeClass('d-none');
		$('.steps-header li:nth-child(3)').removeClass('active');
	}

	//special case
	var mq = window.matchMedia( "(max-width: 640px)" );
	if(mq.matches){
		if(frame==2){
			$('.wsteps .steps-header li:nth-child(2)').hide();
			$('.wsteps .steps-header li:nth-child(1)').show();
		}
		if(frame==3){
			$('.wsteps .steps-header li:nth-child(3)').hide();
			$('.wsteps .steps-header li:nth-child(2)').show();
		}
	} else {
		$('.wsteps > .steps-header > li').each(function(index){
			$(this).show();
		})
	}

});

</script>



<!-- lytebox.close(); -->


