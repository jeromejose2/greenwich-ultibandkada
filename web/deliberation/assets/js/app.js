function clickTo(btn, target)
{
	$(btn).click(function()
	{
		$(target).toggleClass("active");
	});
}

clickTo(".burger",".mainNav");
clickTo(".btn-control",".selection-list");

function selectTab( root ){
	$( root +" > li > a").click( function(e){
		e.preventDefault();
		var tab = $( this );

		$( root +" > li > a" ).removeClass("active");
		tab.addClass("active");

		//reset menu
		$( root ).toggleClass("active");

		if(root==".selection-list"){
			$(".txtHolder").text( tab.text() );
		}
		
		//update div.preview
		if(root == ".selection-list") {
			manageTab( tab.parent().index() );
		}
	});
}

selectTab(".selection-list");
selectTab(".mainNav");

///////////////////////////////
//
//	SLIMCROLL
//
///////////////////////////////
$(document).ready(function() {
  $('.inner-content > .container').slimScroll({
    position: 'right',
    height: '380px',
    /*railVisible: true,
    alwaysVisible: true,*/
    railOpacity: 0.3,
    size: '7px',
    color: '#e72c2e',
    alwaysVisible: true
  });

 $('.overlay-preloader').show();
  $('.overlay-preloader').width( window.innerWidth );
  $('.overlay-preloader').height( window.innerHeight );

});

$(window).resize(function(){
  $('.overlay-preloader').width( window.innerWidth );
  $('.overlay-preloader').height( window.innerHeight );
});


///////////////////////////////
//
//	LYTEBOX
//
///////////////////////////////
var lytebox = new Lytebox();
$('.spanGreen').click(function() {
	lytebox.load({url:'terms.php', bgClose : false });
});



///////////////////////////////
//
//	WAYPOINT
//
///////////////////////////////


//splash - home
var padding = 0;
var section = "";
function scrollTo (id) {
    section = $(id).attr('href');
    targetPixel = $(section).offset().top;
    
  	if(section!="#splash")
    {
        targetPixel=targetPixel - padding;
    } else {
    	section = "#home";
        targetPixel = 0;
    }
    
    $('html, body').animate({scrollTop : targetPixel });
    window.location.hash = section.replace('#','!');
}

var mainNav_li_a = $(".mainNav > li > a");
	
	mainNav_li_a.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var arrowDown = $(".btn.explore");
	arrowDown.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var gallery = $(".btn-view-gallery");
	gallery.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

var registration = $(".regStatus");
	registration.click( function(e){
		e.preventDefault();
		scrollTo(this);
	});

function changeContent( targetId ) {
	var div, idx;

	$(".preview > div").each(function(index) {
		div = $(this);
		idx = div.data("index");

		if(idx == targetId ) div.show();
		else				 div.hide();
	});
}

changeContent(0);//initial

function manageTab( index ){
	changeContent( index );
}






/////////////////////////////////////////////
//			P H A S E 3
/////////////////////////////////////////////

var _self_variable_old_innerHeight2 = 0;
var _self_variable_old_innerHeight1 = 0;

window.onload = function(){
	$('.overlay-preloader').hide();

	var forceScrollToGallery="<a href='#gallery'>";
	$('.list-wrapper > li > a').click( function(e){
		scrollTo(forceScrollToGallery);

		_self_variable_old_innerHeight1 = $('.gallery-wrapper').height();

		$('.gallery-inner').removeClass('animateOut');
		$('.gallery-inner').addClass('animateIn');

		resizeGallery1();
	});

	$('.btn-backtoalbum').click( function(e){
		$('.gallery-inner').removeClass('animateIn');
		$('.gallery-inner').addClass('animateOut');

		$('.gallery-wrapper').height( _self_variable_old_innerHeight1 );
	});

	var forceScrollToFinalist="<a href='#finalist'>";
	$('.list-wrapper2 .btn-view-profile').click( function(e){
		scrollTo(forceScrollToFinalist);

		$('.finalist-inner').removeClass('animateOut');
		$('.finalist-inner').addClass('animateIn');

		_self_variable_old_innerHeight2 = $('.finalist-wrapper').height();
		resizeGallery2();
	});

	$('.btn-backto-lists').click( function(e){
		$('.finalist-inner').removeClass('animateIn');
		$('.finalist-inner').addClass('animateOut');

		$('.finalist-wrapper').height( _self_variable_old_innerHeight2 );
	});

	updateResize();
}

function resizeGallery1(){
	var wrapperH =$('.icontent').height();
	$('.gallery-wrapper').height( wrapperH );
}

function resizeGallery2(){
	var wrapperH =$('.finalist-inner').height();
	$('.finalist-wrapper').height( wrapperH );
}


$('.imglist > .item-img > a').click(function() {
	lytebox.load({url:'popupEntries.php', bgClose : false });
});

window.onresize = updateResize();

function updateResize(){
	//IMPORTANT
	var wrapperH =$('.finalist-wrapper').height();
	var innerH =$('.finalist-inner').height();
	
	if(wrapperH < innerH){
		/*$('.finalist-wrapper').height( innerH );*/
	} 
	if(wrapperH > innerH) {
		/*$('.finalist-inner').height( wrapperH );*/
	}
}

$('._choose').click(function() {
	/*lytebox.load({url:'popupChoose.php', bgClose : false });*/
});

////////////////////////////////
//
//	POP UP ADDITIONAL 
//
////////////////////////////////

$(document).ready(function() {
  $("#comp-list").owlCarousel({
      autoPlay: false, //3000 Set AutoPlay to 3 seconds
      items : 3,
      navigationText: ["",""],
      navigation : true,
      itemsMobile:	[479,1],
      itemsTablet:	[1024,3]
  });
});

