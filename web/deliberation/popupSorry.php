<section class="sorry-message">
	<a href="javascript:void(0);" class="btn-close" onclick="lytebox.close();"></a>

	<div class="lbl-title">SORRY!</div>
	<span class="lbl-sub">You have already voted today. Please try again tomorrow.</span>

</section>