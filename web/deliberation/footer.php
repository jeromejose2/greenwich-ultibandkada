
  <script src="assets/vendors/jquery/jquery.js"></script>
  
  <!-- slimScroll -->
  <script src="assets/vendors/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- lytebox -->
  <script src="assets/vendors/lytebox/js/lytebox.2.3.js"></script>
  <!-- waypoint -->
  <script src="assets/vendors/imakewebthings/waypoints.js"></script>
  <!--carousel-->
  <script src="assets/vendors/owl-carousel/owl.carousel.min.js"></script>

  <script src="assets/js/app.js"></script>

  <footer>
  	<div class="container">
  		&#169; GREENWICH. ALL RIGHTS RESERVED.
  	</div>
  </footer>
</body>
</html>
