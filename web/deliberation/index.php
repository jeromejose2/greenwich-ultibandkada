<?php include 'header.php' ?>
 
  <section id="splash">

    <div class="container">

      <div class="leftCont">
        <img src="assets/img/hashBandkada.png">
        <br/>
        <img class="partners" src="assets/img/partnerLogo.png">
      </div>

      <div class="rightCont">
        <!-- <img src="assets/img/ytsample.jpg"> -->
        <!-- <iframe width="560" height="349" src="//www.youtube.com/embed/itw2kPUFXKE?wmode=opaque&amp;feature=player_detailpage" frameborder="0" allowfullscreen></iframe>-->
      </div>

      <div class="clearfix"></div>

        <div class="desc-wrap">

          <div class="topDescription">

            <h1 class="whiteRed" title="Find out our">Find out our</h1>
            
            <h1 class="whiteRed" title="8 #UltimateBandkada finalists!">8 #UltimateBandkada finalists!</h1>
            
            <h1 class="redWhite" title="April 23, 2015">April 23, 2015</h1>

            <div class="clearfix"></div>

          </div>

        <div class="clearfix"></div>
        </div>

        <a href="#gallery" class="btn-view-gallery">VIEW OUR #ULTIMATEBANDKADA GALLERY HERE</a>

        <div class="clearfix"></div>
    </div>

    <a href="#mechanics" class="btn explore"></a>
    <!-- <a href="#gallery" class="btn explore"></a>  -->
    
    <div class="clearfix"></div>
  </section>

  <div id="mechanics">

    <div class="container omit">

      <h1 class="title">TWO WAYS TO JOIN</h1>

        <div class="firstStepCont special">

            <ul class="steps">
              <li class="s-header">
                <span class="orientHead">ONLINE</span>
              </li>
              <li>
                <div class="innerCont">
                  <!-- <span class="circle">1</span> -->
                  
                  <div class="img"><img src="assets/img/step1-illust.png"></div>
                  <div class="desc">RECORD</div>
                  <div class="clearfix"></div>
                  
                  <p>Choose a song and take a video of your band performing a 1-minute mash-up of at least 2 musical genres. Choose among Rock, Pop, Hip-Hop and EDM.</p>
                  <div class="clearfix"></div>
                </div>
              </li>
              
              <li>
                <div class="innerCont">
                  <!-- <span class="circle">2</span> -->
                  
                  <div class="img"><img src="assets/img/step2-illust.png"></div>
                  <div class="desc">UPLOAD</div>
                  <div class="clearfix"></div>
                  
                  <p>Upload your video on the site (www.ultimatebandkada.com).</p>
                  <div class="clearfix"></div>
                </div>
              </li>
              
              <li>
                <div class="innerCont">
                  <!-- <span class="circle">3</span> -->
                  
                  <div class="img p25"><img src="assets/img/step3-illust.png"></div>
                  <div class="desc p75">REGISTER</div>
                  <div class="clearfix"></div>
                  
                  <p>Make your entry official by completing this <a class="regStatus" href="#registration">form</a> between March 11 and 21. A notification will be sent via email upon successful registration.</p>
                  <div class="clearfix"></div>
                </div>
              </li>
            
            <div class="clearfix"></div>
            </ul>

        </div>

        <div class="orCont">
          <span class="or_c">
            OR
          </span>
        </div>

        <div class="firstStepCont special">
          <div class="joinLive">

              <div class="s-header"> <span class="orientHead">LIVE</span> </div>

              <div class="content">
                Join one of the live auditions from March 16 - 21, 2015
              </div>
              <span class="seefull">See Full Mechanics below.</span>

              <div class="clearfix"></div>
          </div>
        </div>

   <!--  end container -->
    </div>

    <div id="box-cont">
      <div class="box _red"></div>
      <div class="box _green"></div>
      
      <div class="box _white">

        <div class="category">

          <div class="f-mechanics">

            <div class="img-desktop">
              <img src="assets/img/full-mech-desktop.png">
            </div>

          </div>

          <div class="t-label">TOPICS</div>
          
          <div class="custom-select">

            <div class="btn-control">
              <div class="btn-select"></div>
              <span class="txtHolder">Please select ...</span>
            </div>

            <ul class="selection-list"> <!-- active -->

              <li><a href="#" class="active">The #UltimateBandkada Search Contest Overview</a></li>
              
              <li><a href="#">How to Join</a></li>
              
              <li><a href="#">How to Win</a></li>
              
              <li><a href="#">Online Submission</a></li>
              
              <li><a href="#">Live Auditions</a></li>
              
              <li><a href="#">Criteria for Selecting the Eight (8) Bandkada Finalists</a></li>

              <li><a href="#">Criteria for Judging the top 3 Bandkada Finalists and Winners</a></li>

              <!-- <li><a href="#">Event Waiver and Release Form</a></li>-->
              
            </ul>

          </div> 

        <div class="clearfix"></div>
        </div>

        <div class="preview">

          <div class="inner-content" data-index="0">
            
            <div class="container">

              <div class="in-title">The #UltimateBandkada Search Contest Overview</div> 
              <br />
              <p>Become part of the next #UltimateBandkada by teaming up with the hottest music talents in Rock, Pop, Hip Hop and EDM for a chance to win P100,000 plus a recording contract with a major label.</p>
              <br />
              <span>Prizes:</span>
              <span>1<sup>st</sup> Place – P100,000 + management contract + recording contract</span>
              <span>2<sup>nd</sup> Place – P60,000</span>
              <span>3<sup>rd</sup> Place – P40,000</span>
              <br />
              <span>Eligibility of Participants:</span>
              <ul class="decimal">
                <li>Interested groups should be composed of a minimum of three (3) members up to a maximum of five (5) members (all male/female or mixed, Filipino, 18 to 35 years of age upon joining date, residing in Metro Manila) are qualified to join.</li>
                
                <li>To participate in the The #UltimateBandkada Search, interested and qualified groups must comply with the following:
                    <ul class="lower-latin">
                      <li>Visit The #UltimateBandkada Search website (www.ultimatebandkada.com).</li>
                      <li>Have read the full mechanics and registered through the mentioned website.
                        <ul class="lower-roman">
                        
                          <li>A representative of the group may only register a group once and submit a video entry via the registration page of the website (see online submission details).</li>
                        
                        </ul> 
                      </li>
                    
                    </ul>
                </li>

                <li>Hopeful contestants must be well versed on different genres specifically Rock, Pop, Hip Hop or EDM (Electronic Dance Music) disciplines.</li>
              
              </ul>
            </div>
              
          </div>

          <div class="inner-content" data-index="1">
            
            <div class="container">
              <div class="in-title">How to Join</div>
              <br/>
              <ul class="decimal">
                <li>Contestants can only join the contest via two audition methods: Online Submission and Live Audition.</li>
                <li>All submitted online and live auditions from March 11 to March 21, 2015 will be included for the preliminary review.</li>
                <li>A panel of judges will review all auditions to determine eight (8) Bandkada finalists</li>
                <li>Top 3 Bandkada will be awarded in the finale</li>
              </ul>
            </div>

          </div>

          <div class="inner-content" data-index="2">
              <div class="container">
                <div class="in-title">How to Win</div>
                <br />

                <ul class="decimal">
                  <li>Eight Bandkada finalists will be selected from the pool of submitted entries as the finalists who will perform at the culminating event in August 2015.</li>
                  <li>Once formed, a Bandkada must create an original Greenwich theme song that contains elements from at least two (2) different genres the band used for audition. </li>
                  <li>All eight Bandkada finalists will perform their original composition in the grand finale concert event in front of a panel of judges and experts.</li>
                  <li>The top 3 BandKada finalists, including the winner, will be determined by the judges’ score and online voting.</li>
                </ul>

              </div>
          </div>

          <div class="inner-content" data-index="3">

              <div class="container">

                <div class="in-title">Online Submission</div>
                <br />

                <span>Video Submission</span>
                <ul class="decimal">
                  
                  <li>Take a 1-minute video of your band performing a song that incorprates two or more musical genres(rock, hiphop, pop, EDM, etc.).</li>
                  <li>The designated representative must visit the #UltimateBandkada Search website <b>(www.ultimatebandkada.com)</b> and register the following information below:
                    <ul class="lower-latin">
                        <li>Band Name</li>
                        <li>Genre</li>
                        <li>Link to their band site(if any)</li>
                        <li>Name of Representative</li>
                        <li>Birthday</li>
                        <li>Mobile</li>
                        <li>Email</li>
                        <li>Instrument</li>
                        <li>Are you representing a school? If yes, what school?</li>
                        <li>Name of Band Members, instruments, and birthdates.</li>
                        <li>YouTube Channel of the Band</li>
                    </ul>
                  </li>
                  <li>Participants should then upload their entries upon registration with the following video upload guidelines:
                    <ul class="lower-latin">
                        <li>Maximum File Size of 300MB</li>
                        <li>Video File Format: .mp4</li>
                        <li>Video Length: 1 minute</li>
                    </ul>      
                  </li>
                  <li>Participants who have registered will receive a confirmation email indicating the receipt of their entry.</li>
                </ul>

              </div>
          </div>

          <div class="inner-content" data-index="4">
              <div class="container">

                <div class="in-title">Live Auditions</div>
                <br />

                <span>On-ground live auditions will be setup to help contestants facilitate their entry.</span>
                <br />
                <span>Audition Schedules (to be finalized)</span>

                <ul class="disc">
                  <li>March 16, 2015 – UP Diliman</li>
                  <li>March 17, 2015 – Ateneo de Manila University</li>
                  <li>March 18, 2015 - Selected areas near the Manila University Belt (Espana – Sampaloc – Retiro area)</li>
                  <li>March 19, 2015 – De La Salle University</li>
                  <li>March 20, 2015 – University of Asia and the Pacific</li>
                  <li>March 21, 2015 – Greenwich Office - - 32nd Floor Jollibee Plaza Emerald Ave. Ortigas Center, Pasig City.</li>
                </ul>

                <br/>

                <ul class="decimal">
                  <li>Eligible contestants can join a live audition.</li>
                  <li>During the live audition, contestants will be asked to accomplish the registration form and sign a waiver. (See attached registration and waiver form).</li>
                  <li>During a live audition, the Greenwich crew will record a contestant’s performance and will be included in the database.</li>
                  <li>Audition materials shall be owned by Greenwich</li>
                </ul>

              </div>
          </div>

          <div class="inner-content" data-index="5">
              
              <div class="container">
                
                <div class="in-title">Criteria for Selecting the Eight (8) Bandkada Finalists</div>
                <br />

                <span><b>Musicianship:</b> Band members are skilled in playing an instrument that goes with the genres chosen: 25%</span>
                <span><b>Charisma:</b> Contestants have an energetic and attractive personality: 25%</span>
                <span><b>Voice Quality:</b> Contestants have the vocal range to perform quality performances: 25%</span>
                <span><b>Music Mash-Up:</b> The way in which a band has seamlessly and beautifully blended together at least two different music genres to create an entertaining sound: 25%</span>
              
              </div>
          </div>


          <div class="inner-content" data-index="6">
              
              <div class="container">
                
                <div class="in-title">Criteria for Judging the top 3 Bandkada Finalists and Winners</div>
                <br/>

                <span><b>Overview</b></span>
                <br />
                
                <p>All eight (8) BandKada finalists will perform live to determine the winner and top 3 based on the judges’ score.</p>
                <br />
                
                <span>Beats, melodies and notes</span>
                <p>Each Bandkada must compose an original beat and melody for their song. The Bandkada finalists cannot use “remixes” or “samples” of existing songs for their original theme.</p>
                <br />

                <span><b>Criteria for Judging</b></span>
                <br />

                <span>20% Originality – Song uses original beats and melodies</span>
                <span>30% Genre Mash Up – Incorporating at least two more different genres - Pop, Rock, Hip Hop and EDM to create an original music</span>
                <span>20% Audience impact – Crowd reaction during and after band’s performance.</span>
                <span>15% Chemistry – Shows a harmonious relationship and interaction between band members as they play on stage.</span>
                <span>15% Stage Presence – Impressive appearance and performance</span>

                <br />

                <span>ONLINE voting – The band with the most online votes (via app-based purchases) earns an extra 25 points to their overall score.</span>
                <br>

                <span>How to vote:</span>

                  <ul class="decimal">
                    <li>Starting 10:00am of May 1, 2015 until 11:59pm of July 31, 2015, fans may vote their favorite Bandkada for the selected Top 8 through the #UltimateBandkada Search website <b>(www.ultimatebandkada.com)</b>.</li>
                    <li>Fans may vote one (1) band per day through the following steps:
                       <ul class="lower-latin">
                          
                          <li>Fan clicks on the vote button of the selected bandkada.</li>
                          <li>A Facebook Connect prompt appears to track voting via their Facebook account.</li>
                          <li>Once successful, a pop-up appears and asks for a promo code.</li>
                          <li>If no promo code is selected, one (1) vote is counted for the selected band for the day.</li>
                          <li>Fans can repeat the process the day after but will not go through the FB connect process anymore.</li>

                        </ul>
                    </li>

                    <li>Fans can also give additional five (5) votes per coupon code that they get by purchasing a Greenwich Group Meal and ten (10) votes by purchasing a Mash Up Meal which will be available during the voting period.
                    
                      <ul class="lower-latin">
                        
                        <li>Fan clicks on the vote button of the selected bandkada.</li>
                        <li>A Facebook Connect prompt appears to track voting via their Facebook account.</li>
                        <li>Once successful, a pop-up appears and asks for a promo code.</li>
                        <li>If a promo code is selected, the 1st free vote is credited to the chosen bandkada and the fan is asked to upload the coupon code.</li>
                        <li>After the code is validated, first time voters will have to fill up a registration form with the following details:
                            <ul class="lower-roman">
                              <li>Name</li>
                              <li>Email</li>
                              <li>Valid Address</li>
                              <li>Mobile</li>
                            </ul>
                        </li>
                        
                        <li>Upon successful registration, five (5) or ten (10) additional votes depending on the meal purchased will be given to the chosen bandkada/s.</li>
                        <li>The voter can repeat the process as long as he has a valid code but will not have to go through the FB connect and registration process anymore.</li>
                      
                      </ul>
                   </li>

                </ul>

              </div>
          </div>

          <div class="inner-content" data-index="7" style="display:none">
          
            <div class="container">
              <div class="in-title">Event Waiver and Release Form</div>
              <br/>

              <ul class="decimal">
                <li>I submit to the rules, regulations, guidelines and instructions by the sponsors and organizers of the event. </li>
                <li>I agree to cooperate and make myself available for any promotional and/or advertising production, appearance or activity related to the event and give the sponsors and organizers of the event and their assigns full permission, right, and authority to use my name, person, pictures and/or likeness and any other information furnished by me in any manner they deem fit for advertising and promotion; and I, participating in the event, hereby release and discharge the sponsors and organizers of the event from any liability or obligation arising from or connected with such use, including, without limitation, claims for invasion of privacy or defamation. I also understand that I will not be compensated or be paid in the on-ground activation, and that we will be included, seen or caught on cam during advertisements which are crucial for the promotions of the said event.</li>
                <li>ARTIST and ARTIST's bandmates acknowledge that as a condition precedent to their participation in the Search, they are granting the Organizers and Viva Communications, Inc. or any of their respective subsidiaries and affiliates an option to enter into an Agency and Management Agreement and a Multi-Channel Network Affiliation Agreement with the ARTIST and ARTIST's bandmates in such form as the organizers may determine and that the failure to execute the Agency and Management Agreement and Multi-Channel Network Affiliation Agreement will result in disqualification from the Search.</li>
                <li>I hereby warrant that I hold Greenwich Philippines, it’s brand, and as well as Activations Advertising Incorporated, the administrators, employees and organizers absolutely and completely free from any liabilities, claims, injuries or losses attributable to or resulting from the Greenwich The Ultimate Bandkada Search. I execute this waiver to attest to the truth of the foregoing and for whatever legal purpose this may serve.</li>
              </ul>
          
            </div>
          
          </div>
          
        <!-- preview-end  -->
        </div>

        <div class="clearfix"></div>
      </div>
      
      <div class="clearfix"></div>
    </div>

  <!-- end mechanics -->
  <!-- <div class="clearfix"></div> -->
  </div>

  

<div id="gallery">

    <div class="banner-gallery"> <img src="assets/img/gallery-banner.png"> </div>

    <div class="gallery-wrapper">

        <div class="content">

          <div class="select-wrap">
              <select>
                <option value="value">SORT BY NEWEST TO OLDEST</option>
                <option value="value">NEW</option>
                <option value="value">OLD</option>
            </select>
          </div>

          <div class="lbl-album">ALBUM LIST</div>

          <ul class="list-wrapper">
            
            <!--1-->
            <li class="item">
              <a href="javascript:void(0);">
                <div class="dateholder">MARCH 08, 2015</div>
                <img src="assets/img/home-gallery-img01.jpg">
                <div class="desc">
                  
                  <div class="inner-cont">
                    <p>Bandkada session at The Philippine arena</p>
                  </div>
                </div>
              </a>
            </li>

          <div class="clearfix"></div>
          </ul>

          <div class="pagination-wrap">
            <a href="javascript:void(0);" class="btn-page-left"></a>
            <a href="javascript:void(0);" class="btn-page-right"></a>
          </div>
        <!-- content -->
        </div>

       <!--  hide section -->
        <div class="gallery-inner"> <!-- min-height: 750px; -->

            <div class="icontent">
                <div class="lbl-title">BANDKADA SESSION AT THE PHILIPPINE ARENA</div>
                <div class="lbl-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>
                
                <a class="btn-backtoalbum" href="javascript:void(0);"></a>

            <ul class="imglist">
              <!--1-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--2-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--3-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--4-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>

              <!--1-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--2-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--3-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>
              <!--4-->
              <li class="item-img spl-square">
                <a href="javascript:void(0);">
                  <img src="assets/img/home-gallery-img01.jpg">
                </a>
              </li>

            <div class="clearfix"></div>
            </ul>
            
            <a href="javascript:void(0);" class="btn-ifb"></a>
            <a href="javascript:void(0);" class="btn-itw"></a>
            
            <div class="inner-pagination">
              <a href="javascript:void(0);" class="btn-page-left"></a>
              <a href="javascript:void(0);" class="btn-page-right"></a>
            </div>

            <div class="clearfix"></div>

            <!--icontent-->
            </div>
        <!-- inner-gallery -->
        </div>
    <!-- gallery-wrapper -->
    </div>
 <!-- gallery -->   
</div>

<div id="finalist">
  <div class="bottomdes"></div>

  <div class="banner-finalist">
    <img src="assets/img/finalist-banner.png">
  </div>

  <div class="finalist-wrapper">
  
    <div class="content">

      <ul class="list-wrapper2">
        
        <!--1-->
        <li class="item">
          
          <div class="votesholder dateholder">1,000 VOTES</div>
          <img src="assets/img/finalist-img01.jpg">

          <div class="desc">
            <div class="inner-cont">
              <span class="lbl-title">BANDARITAS</span>
              <p class="info">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
              <a class="btn-view-profile" href="javascript:void(0);"></a>
              <a class="btn-vote-profile" href="javascript:void(0);" onclick="lytebox.load({url:'popupChoose.php', bgClose : false });"></a>
            </div>
          </div>
        </li>

        <div class="clearfix"></div>
      </ul>
      <!-- content end -->
    </div>

    <!-- INNER PAGE -->
    <div class="finalist-inner">

        <div class="inner-portfolio-wrapper">
          
          <div class="album-img"></div>
          
          <a href="javascript:void(0);" class="btn-backto-lists" href="javascript:void(0);"></a>
  
          <div class="side-desc">
          
            <span class="lbl-group-name">MOONANDSTAR</span>
          
            <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br/><br/>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </div>
          
          </div>

          <div class="clearfix"></div>

          <div class="vote-content">
            
            <a href="javascript:void(0);" class="btn-spl-vote" onclick="lytebox.load({url:'popupChoose.php', bgClose : false });"></a>
            <div class="votes-indicator">
              <div class="content">
                <span class="votes-count">1,000 votes</span>
              </div>
            </div>

          <div class="clearfix"></div>
          </div>

          <div class="clipvideo">
             <iframe width="984" height="516" src="//www.youtube.com/embed/itw2kPUFXKE?wmode=opaque&amp;feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
          </div>

        <div class="clearfix"></div>
        <!-- inner-portfolio-wrapper -->
        </div>

     <!-- finalist-inner -->
    </div>

  <!-- finalist-wrapper -->
  </div>
<!-- finalist -->
</div>


<?php include 'footer.php' ?>
