<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>The #UltimateBandkada Search</title>

  <!-- 
  <link href="assets/vendors/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
   -->

  <!--lytebox-->
  <link href="assets/vendors/lytebox/css/lytebox.css" rel="stylesheet">
  <link href="assets/vendors/font/fontLib.css" rel="stylesheet">

  <link href="assets/vendors/owl-carousel/owl.carousel.css" rel="stylesheet">
  <link href="assets/vendors/owl-carousel/owl.theme.css" rel="stylesheet">
  <link href="assets/vendors/owl-carousel/owl.transitions.css" rel="stylesheet">  

  <link rel="stylesheet" href="assets/css/main.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="icon" href="gwlogo.ico" type="image/x-icon" />

</head>

<body>

<div class="overlay-preloader">
  <div class="loader-wrap">
    <img src="assets/img/mpreloader.gif">
  </div>
</div>

<!--[if lt IE 10]>
  <p class="browsehappy">
    You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve
    your experience.
  </p>
<![endif]-->

<div class="header">
  <div class="container">

    <span class="f-us">FOLLOW US</span>
    
    <ul class="socialNav">
      <li><a target="_blank" href="https://www.facebook.com/GreenwichPizza" class="btn icon-fb spcTop"></a></li>
      <li><a target="_blank" href="https://twitter.com/greenwichpizza/" class="btn icon-tw spcTop" ></a></li>
      <li><a target="_blank" href="https://instagram.com/greenwichpizza/" class="btn icon-ins spcTop"></a></li>
      <li class="acc"><span>/GREENWICHPIZZA</span></li>
    </ul>

    <ul class="mainNav"> <!-- active -->
        <li>
          <a href="#splash" class="active">HOME</a>
        </li>
       
        <li>
          <a href="#mechanics">MECHANICS</a>
        </li>
       
        <li>
          <a href="#gallery">GALLERY</a>
        </li>

        <li>
          <a href="#finalist">FINALISTS</a>
        </li>

        <div class="clearfix"></div>
    </ul>

    <div class="burger">
      <span class="bar"></span>
    </div>

  <div class="clearfix"></div>
  </div>
</div>

<div class="popups">
  <span>POPUPS:</span>
  <a href="javascript:void(0);" class="_choose" onclick="lytebox.load({url:'popupChoose.php', bgClose : false });">[ CHOOSE ]</a>
  <a href="javascript:void(0);" class="_withCoupon" onclick="lytebox.load({url:'steps.php', bgClose : false });">[ WITH_COUPON ]</a>
  <a href="javascript:void(0);" class="_sorry"  onclick="lytebox.load({url:'popupSorry.php', bgClose : false });">[ SORRY ]</a>
  <a href="javascript:void(0);" class="_thankyou"  onclick="lytebox.load({url:'popupThankyou.php', bgClose : false });">[ THANK YOU ]</a>
  <a href="javascript:void(0);" class="_waiver"  onclick="lytebox.load({url:'popupWaiver.php', bgClose : false });">[ WAIVER ]</a>
</div>

<!-- slimscroll
     section addendum -->
