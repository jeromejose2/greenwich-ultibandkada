<section class="img-entry">
	
	<a href="javascript:void(0);" class="btn-close" onclick="lytebox.close();"></a>

	<div class="content">
		<img src="assets/img/popUpsampleImg.png">
   	</div>

   	<div class="social-wrap">
   		<a href="javascript:void(0);" class="btn-ifb"></a>
      <a href="javascript:void(0);" class="btn-itw"></a>
   	</div>

   	<div class="pagination-wrap">
      <a href="javascript:void(0);" class="btn-page-left"></a>
      <a href="javascript:void(0);" class="btn-page-right"></a>
   	</div>

   	<div class="clearfix"></div>
</section>


<script type="text/javascript">
  
  $(document).ready( function() {
    var tick = setTimeout(function(){
      clearTimeout(tick);
      lytebox.stabilize();
    }, 100 );
  } );

</script>