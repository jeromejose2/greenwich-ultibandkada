<?php

namespace CI\BandkadaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="CI\BandkadaBundle\Entity\AlbumRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class Album extends BaseEntity
{
	/**
	 * @ORM\Column(name="name", type="string", length=36)
	 * @Assert\Length(min=1, max=36)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Name is required.")
	 */
	protected $name;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="event_date", type="date")
	 * @Assert\NotBlank(message="Event date is required.")
	 * @Assert\Date()
	 */
	private $eventDate;
	
	/**
	 * @ORM\Column(name="description", type="text")
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Description is required.")
	 */
	protected $description;
	
	/**
	 * @ORM\OneToMany(targetEntity="Media", mappedBy="album", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"id" = "ASC"})
	 * @Assert\Count(min="1", minMessage="You must add at least one media.")
	 * @Assert\Valid
	 */
	protected $media;
	
	/**
	 * @ORM\Column(name="filename", type="string", length=255)
	 */
	private	$filename;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="300M")
	 */
	private $file;
	
	private $temp;
	
	private $changeImage;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->media = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	public function getAbsolutePath()
	{
		return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
	}
	
	public function getWebPath()
	{
		return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/albums';
	}
	
	/**
	 * Sets changeImage
	 *
	 * @param boolean $deleteImage
	 */
	public function setChangeImage($changeImage)
	{
		$this->changeImage = $changeImage;
	
		return $this;
	}
	
	public function getChangeImage()
	{
		return $this->changeImage;
	}

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     * @return Album
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;
    
        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime 
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Album
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add media
     *
     * @param \CI\BandkadaBundle\Entity\Media $media
     * @return Album
     */
    public function addMedium(\CI\BandkadaBundle\Entity\Media $media)
    {
    	$media->setAlbum($this);
        $this->media[] = $media;
    
        return $this;
    }

    /**
     * Remove media
     *
     * @param \CI\BandkadaBundle\Entity\Media $media
     */
    public function removeMedium(\CI\BandkadaBundle\Entity\Media $media)
    {
        $this->media->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedia()
    {
        return $this->media;
    }
    
    /**
     * Set filename
     *
     * @param string $filename
     * @return Album
     */
    public function setFilename($filename)
    {
    	$this->filename = $filename;
    
    	return $this;
    }
    
    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
    	return $this->filename;
    }
    
    /**
     * Set path
     *
     * @param string $path
     * @return Album
     */
    public function setPath($path)
    {
    	$this->path = $path;
    
    	return $this;
    }
    
    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
    	return $this->path;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
    	$this->file = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath())) {
    		// store the old name to delete after the update
    		$this->temp = $this->getAbsolutePath();
    	} else {
    		$this->path = 'initial';
    	}
    }
    
    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if (null !== $this->getFile()) {
    		// do whatever you want to generate a unique name
    		$this->setFilename($this->getFile()->getClientOriginalName());
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->path = $filename . '.' . $this->getFile()->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	 
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	 
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		unlink($this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	 
    	$this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	if (!empty($this->file)) {
    		if ($this->checkExtension($this->file->guessExtension()) === false) {
    			$context->addViolationAt("file", "Invalid image format uploaded.");
    		} else {
    			// this updates the file incase no other fields are updated.
    			$this->setUpdatedAt(new \DateTime('now'));
    		}
    	} else {
    		if (!$this->getId() || ($this->getId() && $this->getChangeImage())) {
    			$context->addViolationAt('file', 'Album cover is required.');
    		}
    	}
    }
    
    private function checkExtension($extension) 
    {
    	$allowedExtensions = array(
    		'jpeg', 'png', 'jpg'
    	);
    	return in_array($extension, $allowedExtensions);
    }
    
    public function getPaginatedMedia($countPerPage)
    {
    	if (empty($countPerPage)) {
    		$countPerPage = $this->getMedia()->count();
    	}
    	
    	$count = 1;
    	$pageNo = 0;
    	$pages = array();
    	
    	foreach($this->getMedia() as $media) {
    		if ($count % $countPerPage === 1) {
    			$pageNo++;
    		}
    		
    		$pages[$pageNo][] = $media;
    		$count++;
    	}
    	
    	return $pages;
    }
}