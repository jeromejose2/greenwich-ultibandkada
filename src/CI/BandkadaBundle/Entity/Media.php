<?php

namespace CI\BandkadaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity(repositoryClass="CI\BandkadaBundle\Entity\MediaRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class Media extends BaseEntity
{
	/**
	 * @ORM\Column(name="youtube_video_embed_code", type="text", nullable=true)
	 */
	private $youtubeVideoEmbedCode;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Album", inversedBy="media")
	 */
	private $album;
	
	/**
	 * @ORM\Column(name="image_filename", type="string", length=255)
	 */
	private	$imageFilename;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
     * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="300M")
	 */
	private $file;
	
	private $temp;
	
	private $changeImage;
	
	public function getAbsolutePath()
	{
		return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
	}
	
	public function getWebPath()
	{
		return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/media';
	}
	
	/**
	 * Sets changeImage
	 *
	 * @param boolean $deleteImage
	 */
	public function setChangeImage($changeImage)
	{
		$this->changeImage = $changeImage;
	
		return $this;
	}
	
	public function getChangeImage()
	{
		return $this->changeImage;
	}
	
	/**
	 * Set youtubeVideoEmbedCode
	 *
	 * @param string $youtubeVideoEmbedCode
	 * @return Media
	 */
	public function setYoutubeVideoEmbedCode($youtubeVideoEmbedCode)
	{
		$this->youtubeVideoEmbedCode = $youtubeVideoEmbedCode;
	
		return $this;
	}
	
	/**
	 * Get youtubeVideoEmbedCode
	 *
	 * @return string
	 */
	public function getYoutubeVideoEmbedCode()
	{
		return $this->youtubeVideoEmbedCode;
	}
	
	/**
	 * Set path
	 *
	 * @param string $path
	 * @return Media
	 */
	public function setPath($path)
	{
		$this->path = $path;
	
		return $this;
	}
	
	/**
	 * Get path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}
	
	/**
	 * Set album
	 *
	 * @param \CI\BandkadaBundle\Entity\Album $album
	 * @return Media
	 */
	public function setAlbum(\CI\BandkadaBundle\Entity\Album $album = null)
	{
		$this->album = $album;
	
		return $this;
	}
	
	/**
	 * Get album
	 *
	 * @return \CI\BandkadaBundle\Entity\Album
	 */
	public function getAlbum()
	{
		return $this->album;
	}

    /**
     * Set imageFilename
     *
     * @param string $imageFilename
     * @return Media
     */
    public function setImageFilename($imageFilename)
    {
    	$this->imageFilename = $imageFilename;
    
    	return $this;
    }
    
    /**
     * Get imageFilename
     *
     * @return string
     */
    public function getImageFilename()
    {
    	return $this->imageFilename;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
    	$this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->path = 'initial';
        }
    }
    
    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $this->setImageFilename($this->getFile()->getClientOriginalName());
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getFile()->guessExtension();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		unlink($this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	
    	$this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	
    	if (!empty($this->file)) {
    		if ($this->checkExtension($this->file->guessExtension()) === false) {
    			$context->addViolationAt("file", "Invalid image format uploaded.");
    		} else {
    			// this updates the file incase no other fields are updated.
    			$this->setUpdatedAt(new \DateTime('now'));
    		}
    	} else {
    		if (!$this->getId() || ($this->getId() && $this->getChangeImage())) {
    			$context->addViolationAt('file', 'Image is required.');
    		}
    	}
    } 
    
    private function checkExtension($extension) {
    	 
    	$allowedExtensions = array(
    			'jpeg', 'jpg', 'png'
    	);
    	return in_array($extension, $allowedExtensions);
    }
}