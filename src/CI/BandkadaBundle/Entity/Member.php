<?php

namespace CI\BandkadaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Member
 *
 * @ORM\Table(name="member")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class Member
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\Column(name="name", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\Regex(
	 * 		pattern="/^[a-z][a-z ]*$/i",
	 * 		match=true,
	 * 		message="Only letters and spaces are allowed.")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(name="instrument", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	protected $instrument;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="birthday", type="date")
	 * @Assert\Date()
	 */
	private $birthday;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Band", inversedBy="members")
	 */
	private $band;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Member
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set instrument
     *
     * @param string $instrument
     * @return Member
     */
    public function setInstrument($instrument)
    {
        $this->instrument = $instrument;
    
        return $this;
    }

    /**
     * Get instrument
     *
     * @return string 
     */
    public function getInstrument()
    {
        return $this->instrument;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Member
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
    
    /**
     * Set band
     *
     * @param \CI\BandkadaBundle\Entity\Band $band
     * @return Member
     */
    public function setBand(\CI\BandkadaBundle\Entity\Band $band = null)
    {
    	$this->band = $band;
    
    	return $this;
    }
    
    /**
     * Get band
     *
     * @return \CI\BandkadaBundle\Entity\Band
     */
    public function getBand()
    {
    	return $this->band;
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	if ($this->getName() || $this->getBirthday() || $this->getInstrument()) {
    		if (!$this->getName()) {
    			$context->addViolationAt('name', 'Please enter the first and last name.');
    		}
    		
    		if (!$this->getBirthday()) {
    			$context->addViolationAt('birthday', 'Please enter the birthday.');
    		}
    		
    		if (!$this->getInstrument()) {
    			$context->addViolationAt('instrument', 'Please enter the instrument.');
    		}
    		
    		$birthday = $this->getBirthday();
    		if ($birthday) {
    			$today = new \DateTime();
    			$diff = $birthday->diff($today);
    			$years = $diff->format('%y');
    			 
    			if ($years < 18 || $years > 35) {
    				$context->addViolationAt('birthday', 'Members should be between 18 and 35 years old from date of registration.');
    			}
    		}
    		
    	}
    }
}