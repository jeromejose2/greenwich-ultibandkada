<?php

namespace CI\BandkadaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Finalist
 *
 * @ORM\Table(name="finalist")
 * @ORM\Entity(repositoryClass="CI\BandkadaBundle\Entity\FinalistRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 */
class Finalist extends BaseEntity
{
	/**
	 * @ORM\Column(name="name", type="string", length=50)
	 * @Assert\Length(min=1, max=50)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Name is required.")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(name="short_write_up", type="text")
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=70)
	 * @Assert\NotBlank(message="Short write-up is required.")
	 */
	protected $shortWriteUp;
	
	/**
	 * @ORM\Column(name="write_up", type="text")
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Write-up is required.")
	 */
	protected $writeUp;
	
	/**
	 * @ORM\Column(name="youtube_video_embed_code", type="text", nullable=true)
	 */
	private $youtubeVideoEmbedCode;
	
	/**
	 * @ORM\Column(name="teaser_filename", type="string", length=255)
	 */
	private	$teaserFilename;
	
	/**
	 * @ORM\Column(name="teaser_path", type="string", length=255)
	 */
	private	$teaserPath;
	
	/**
	 * @Assert\File(maxSize="300M")
	 */
	private $teaserFile;
	
	private $teaserTemp;
	
	private $teaserChangeImage;
	
	/**
	 * @ORM\Column(name="image_filename", type="string", length=255)
	 */
	private	$imageFilename;
	
	/**
	 * @ORM\Column(name="image_path", type="string", length=255)
	 */
	private	$imagePath;
	
	/**
	 * @Assert\File(maxSize="300M")
	 */
	private $imageFile;
	
	private $imageTemp;
	
	private $imageChangeImage;
	
	public function getAbsolutePath($type)
	{
		$property = $type == 'teaser' ? $this->teaserPath : $this->imagePath;
		return null === $property ? null : $this->getUploadRootDir() . '/' . $property;
	}
	
	public function getWebPath($type)
	{
		$property = $type == 'teaser' ? $this->teaserPath : $this->imagePath;
		return null === $property ? null : $this->getUploadDir() . '/' . $property;
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/finalists';
	}
	
	/**
	 * Sets teaserChangeImage
	 *
	 * @param boolean $teaserChangeImage
	 */
	public function setTeaserChangeImage($teaserChangeImage)
	{
		$this->teaserChangeImage = $teaserChangeImage;
	
		return $this;
	}
	
	public function getTeaserChangeImage()
	{
		return $this->teaserChangeImage;
	}
	
	/**
	 * Sets imageChangeImage
	 *
	 * @param boolean $imageChangeImage
	 */
	public function setImageChangeImage($imageChangeImage)
	{
		$this->imageChangeImage = $imageChangeImage;
	
		return $this;
	}
	
	public function getImageChangeImage()
	{
		return $this->imageChangeImage;
	}

    /**
     * Set name
     *
     * @param string $name
     * @return Finalist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set shortWriteUp
     *
     * @param string $shortWriteUp
     * @return Finalist
     */
    public function setShortWriteUp($shortWriteUp)
    {
    	$this->shortWriteUp = $shortWriteUp;
    
    	return $this;
    }
    
    /**
     * Get shortWriteUp
     *
     * @return string
     */
    public function getShortWriteUp()
    {
    	return $this->shortWriteUp;
    }
    
    /**
     * Set writeUp
     *
     * @param string $writeUp
     * @return Finalist
     */
    public function setWriteUp($writeUp)
    {
    	$this->writeUp = $writeUp;
    
    	return $this;
    }
    
    /**
     * Get writeUp
     *
     * @return string
     */
    public function getWriteUp()
    {
    	return $this->writeUp;
    }
    
    /**
     * Set youtubeVideoEmbedCode
     *
     * @param string $youtubeVideoEmbedCode
     * @return Finalist
     */
    public function setYoutubeVideoEmbedCode($youtubeVideoEmbedCode)
    {
    	$this->youtubeVideoEmbedCode = $youtubeVideoEmbedCode;
    
    	return $this;
    }
    
    /**
     * Get youtubeVideoEmbedCode
     *
     * @return string
     */
    public function getYoutubeVideoEmbedCode()
    {
    	return $this->youtubeVideoEmbedCode;
    }
    
    /**
     * Set teaserFilename
     *
     * @param string $teaserFilename
     * @return Finalist
     */
    public function setTeaserFilename($teaserFilename)
    {
    	$this->teaserFilename = $teaserFilename;
    
    	return $this;
    }
    
    /**
     * Get teaserFilename
     *
     * @return string
     */
    public function getTeaserFilename()
    {
    	return $this->teaserFilename;
    }
    
    /**
     * Set teaserPath
     *
     * @param string $teaserPath
     * @return Finalist
     */
    public function setTeaserPath($teaserPath)
    {
    	$this->teaserPath = $teaserPath;
    
    	return $this;
    }
    
    /**
     * Get teaserPath
     *
     * @return string
     */
    public function getTeaserPath()
    {
    	return $this->teaserPath;
    }
    
    /**
     * Set imageFilename
     *
     * @param string $imageFilename
     * @return Finalist
     */
    public function setImageFilename($imageFilename)
    {
    	$this->imageFilename = $imageFilename;
    
    	return $this;
    }
    
    /**
     * Get imageFilename
     *
     * @return string
     */
    public function getImageFilename()
    {
    	return $this->imageFilename;
    }
    
    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return Finalist
     */
    public function setImagePath($imagePath)
    {
    	$this->imagePath = $imagePath;
    
    	return $this;
    }
    
    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
    	return $this->imagePath;
    }

    /**
     * Sets teaser file.
     *
     * @param UploadedFile $file
     */
    public function setTeaserFile(UploadedFile $file = null)
    {
    	$this->teaserFile = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath('teaser'))) {
    		// store the old name to delete after the update
    		$this->teaserTemp = $this->getAbsolutePath('teaser');
    	} else {
    		$this->teaserPath = 'initial';
    	}
    }
    
    /**
     * Get teaser file.
     *
     * @return UploadedFile
     */
    public function getTeaserFile()
    {
    	return $this->teaserFile;
    }
    
    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
    	$this->imageFile = $file;
    	// check if we have an old image path
    	if (is_file($this->getAbsolutePath('image'))) {
    		// store the old name to delete after the update
    		$this->imageTemp = $this->getAbsolutePath('image');
    	} else {
    		$this->imagePath = 'initial';
    	}
    }
    
    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
    	return $this->imageFile;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
    	if (null !== $this->getTeaserFile()) {
    		// do whatever you want to generate a unique name
    		$this->setTeaserFilename($this->getTeaserFile()->getClientOriginalName());
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->teaserPath = $filename . '.' . $this->getTeaserFile()->guessExtension();
    	}
    	
    	if (null !== $this->getImageFile()) {
    		$this->setImageFilename($this->getImageFile()->getClientOriginalName());
    		$filename = sha1(uniqid(mt_rand(), true));
    		$this->imagePath = $filename . '.' . $this->getImageFile()->guessExtension();
    	}
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null !== $this->getTeaserFile()) {
    		$this->getTeaserFile()->move($this->getUploadRootDir(), $this->teaserPath);
    	 
	    	// check if we have an old image
	    	if (isset($this->teaserTemp)) {
	    		// delete the old image
	    		unlink($this->teaserTemp);
	    		// clear the temp image path
	    		$this->teaserTemp = null;
	    	}
	    	 
	    	$this->teaserFile = null;
    	}
    	
    	if (null !== $this->getImageFile()) {
    		$this->getImageFile()->move($this->getUploadRootDir(), $this->imagePath);
    		
    		if (isset($this->imageTemp)) {
    			unlink($this->imageTemp);
    			$this->imageTemp = null;
    		}
    		 
    		$this->imageFile = null;
    	}
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$teaserFile = $this->getAbsolutePath('teaser');
    	if ($teaserFile) {
    		unlink($teaserFile);
    	}
    	
    	$imageFile = $this->getAbsolutePath('image');
    	if ($imageFile) {
    		unlink($imageFile);
    	}
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	$update = true;
    	
    	if (!empty($this->teaserFile)) {
    		if ($this->checkExtension($this->teaserFile->guessExtension()) === false) {
    			$context->addViolationAt('teaserFile', 'Invalid image format uploaded.');
    		} else {
    			$update = false;
    		}
    	} else {
    		if (!$this->getId() || ($this->getId() && $this->getTeaserChangeImage())) {
    			$context->addViolationAt('teaserFile', 'Teaser image is required.');
    		}
    	}
    	
    	if (!empty($this->imageFile)) {
    		if ($this->checkExtension($this->imageFile->guessExtension()) === false) {
    			$context->addViolationAt('imageFile', 'Invalid image format uploaded.');
    		} else {
    			$update = false;
    		}
    	} else {
    		if (!$this->getId() || ($this->getId() && $this->getImageChangeImage())) {
    			$context->addViolationAt('imageFile', 'Image is required.');
    		}
    	}
    	
    	// this updates the file incase no other fields are updated.
    	if ($update == false) {
    		$this->setUpdatedAt(new \DateTime('now'));
    	}
    }
    
    private function checkExtension($extension) 
    {
    	$allowedExtensions = array(
    		'jpeg', 'png', 'jpg'
    	);
    	return in_array($extension, $allowedExtensions);
    }
}