<?php

namespace CI\BandkadaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Band
 *
 * @ORM\Table(name="band")
 * @ORM\Entity(repositoryClass="CI\BandkadaBundle\Entity\BandRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"process"})
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="This email is already in use."
 * )
 */
class Band extends BaseEntity
{
	const GENRE_ROCK = "Rock";
	const GENRE_POP = "Pop";
	const GENRE_HIPHOP = "Hip Hop";
	const GENRE_EDM = "EDM";
	
	/**
	 * @var boolean
	 * @Assert\True(message="You must agree to the terms and conditions.")
	 */
	protected $acknowledge;
	
	/**
	 * @ORM\Column(name="rep_name", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter your first and last name.")
	 * @Assert\Regex(
	 * 		pattern="/^[a-z][a-z ]*$/i",
	 * 		match=true,
	 * 		message="Only letters and spaces are allowed.")
	 */
	protected $repName;
	
	/**
	 * @ORM\Column(name="mobile_number", type="string", length=11)
	 * @Assert\Length(
	 *		min = 11,
     *		max = 11,
     *		exactMessage = "Mobile numbers should be 11 numbers long."
	 * )
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter your mobile number.")
	 * @Assert\Regex(
     *     pattern="/\d+/",
     *     match=true,
     *     message="Mobile number should only contain numbers."
     * )
	 */
	protected $mobileNumber;
	
	/**
	 * @ORM\Column(name="rep_instrument", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter your instrument.")
	 */
	protected $repInstrument;
	
	/**
	 * @var date
	 *
	 * @ORM\Column(name="rep_birthday", type="date")
	 * @Assert\NotBlank(message="Please enter your birthday.")
	 * @Assert\Date()
	 */
	private $repBirthday;
	
	/**
	 * @ORM\Column(name="email", type="string", length=100)
	 * @Assert\Email(message="Please enter a valid email.")
	 * @Assert\NotBlank(message="Please enter your email.")
	 * @Assert\Length(min=0, max=100)
	 */
	private $email;
	
	/**
	 * @ORM\Column(name="band_name", type="string", length=255)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 * @Assert\NotBlank(message="Please enter your band's name.")
	 */
	private $bandName;
	
	/**
	 * @ORM\Column(name="band_youtube_url", type="string", length=100, nullable=true)
	 * @Assert\Url(message="Please enter a valid url.")
	 * @Assert\Length(min=0, max=100)
	 */
	private $bandYoutubeUrl;
	
	/**
	 * @ORM\Column(name="band_video_link", type="string", length=100, nullable=true)
	 * @Assert\Length(min=0, max=100)
	 * @Assert\Type(type="string")
	 */
	private $bandVideoLink;
	
	/**
	 * @ORM\Column(name="genre", type="array")
	 * @Assert\Choice(choices={"Rock", "Pop", "Hip Hop", "EDM"}, multiple=true, min=1, minMessage="Please choose at least 1 genre.")
	 */
	private $genre;
	
	/**
	 * @ORM\Column(name="school", type="string", length=255, nullable=true)
	 * @Assert\Length(min=1, max=255)
	 * @Assert\Type(type="string")
	 */
	protected $school;
	
	/**
	 * @ORM\OneToMany(targetEntity="Member", mappedBy="band", cascade={"persist"})
	 * @ORM\OrderBy({"name"="ASC"})
	 * @Assert\Valid
	 */
	protected $members;
	
	/**
	 * @ORM\Column(name="file_name", type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
	 * @Assert\Length(max=255)
	 */
	private	$fileName;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Type(type="string")
     * @Assert\Length(max=255)
	 */
	private	$path;
	
	/**
	 * @Assert\File(maxSize="300M")
	 */
	private $file;
	
	private $temp;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->members = new \Doctrine\Common\Collections\ArrayCollection();
	}
	
	public function getAbsolutePath()
	{
		return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
	}
	
	public function getWebPath()
	{
		return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
	}
	
	protected function getUploadRootDir()
	{
		return __DIR__.'/../../../../web/'.$this->getUploadDir();
	}
	
	protected function getUploadDir()
	{
		return 'uploads/bands/' . $this->getId();
	}

	/**
	 * Set acknowledge
	 *
	 * @param boolean $acknowledge
	 * @return Band
	 */
	public function setAcknowledge($acknowledge)
	{
		$this->acknowledge = $acknowledge;
	
		return $this;
	}
	
	/**
	 * Get acknowledge
	 *
	 * @return boolean
	 */
	public function getAcknowledge()
	{
		return $this->acknowledge;
	}
	
    /**
     * Set repName
     *
     * @param string $repName
     * @return Band
     */
    public function setRepName($repName)
    {
        $this->repName = $repName;
    
        return $this;
    }

    /**
     * Get repName
     *
     * @return string 
     */
    public function getRepName()
    {
        return $this->repName;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     * @return Band
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;
    
        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string 
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set repInstrument
     *
     * @param string $repInstrument
     * @return Band
     */
    public function setRepInstrument($repInstrument)
    {
        $this->repInstrument = $repInstrument;
    
        return $this;
    }

    /**
     * Get repInstrument
     *
     * @return string 
     */
    public function getRepInstrument()
    {
        return $this->repInstrument;
    }

    /**
     * Set repBirthday
     *
     * @param \DateTime $repBirthday
     * @return Band
     */
    public function setRepBirthday($repBirthday)
    {
        $this->repBirthday = $repBirthday;
    
        return $this;
    }

    /**
     * Get repBirthday
     *
     * @return \DateTime 
     */
    public function getRepBirthday()
    {
        return $this->repBirthday;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Band
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set bandName
     *
     * @param string $bandName
     * @return Band
     */
    public function setBandName($bandName)
    {
        $this->bandName = $bandName;
    
        return $this;
    }

    /**
     * Get bandName
     *
     * @return string 
     */
    public function getBandName()
    {
        return $this->bandName;
    }

    /**
     * Set bandYoutubeUrl
     *
     * @param string $bandYoutubeUrl
     * @return Band
     */
    public function setBandYoutubeUrl($bandYoutubeUrl)
    {
        $this->bandYoutubeUrl = $bandYoutubeUrl;
    
        return $this;
    }

    /**
     * Get bandYoutubeUrl
     *
     * @return string 
     */
    public function getBandYoutubeUrl()
    {
        return $this->bandYoutubeUrl;
    }
    
    /**
     * Set bandVideoLink
     *
     * @param string $bandVideoLink
     * @return Band
     */
    public function setBandVideoLink($bandVideoLink)
    {
    	$this->bandVideoLink = $bandVideoLink;
    
    	return $this;
    }
    
    /**
     * Get bandVideoLink
     *
     * @return string
     */
    public function getBandVideoLink()
    {
    	return $this->bandVideoLink;
    }

    /**
     * Set genre
     *
     * @param array $genre
     * @return Band
     */
    public function setGenre($genre)
    {
    	$this->genre = $genre;
    
    	return $this;
    }
    
    /**
     * Get genre
     *
     * @return array
     */
    public function getGenre()
    {
    	return $this->genre;
    }

    /**
     * Set school
     *
     * @param string $school
     * @return Band
     */
    public function setSchool($school)
    {
        $this->school = $school;
    
        return $this;
    }

    /**
     * Get school
     *
     * @return string 
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Add members
     *
     * @param \CI\BandkadaBundle\Entity\Member $members
     * @return Band
     */
    public function addMember(\CI\BandkadaBundle\Entity\Member $members)
    {
    	$members->setBand($this);
        $this->members[] = $members;
    
        return $this;
    }

    /**
     * Remove members
     *
     * @param \CI\BandkadaBundle\Entity\Member $members
     */
    public function removeMember(\CI\BandkadaBundle\Entity\Member $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMembers()
    {
        return $this->members;
    }
    
    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Band
     */
    public function setFileName($fileName)
    {
    	$this->fileName = $fileName;
    
    	return $this;
    }
    
    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
    	return $this->fileName;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
    	$this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->path = 'initial';
        }
    }
    
    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
    	return $this->file;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $this->setFileName($this->getFile()->getClientOriginalName());
            $filename = sha1(uniqid(mt_rand(), true));
            $this->path = $filename . '.' . $this->getFile()->guessExtension();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
    	if (null === $this->getFile()) {
    		return;
    	}
    	
    	$this->getFile()->move($this->getUploadRootDir(), $this->path);
    	
    	// check if we have an old image
    	if (isset($this->temp)) {
    		// delete the old image
    		//unlink($this->temp);
    		// clear the temp image path
    		$this->temp = null;
    	}
    	
    	$this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
    	$file = $this->getAbsolutePath();
    	if ($file) {
    		unlink($file);
    	}
    }
    
    public function process(ExecutionContextInterface $context)
    {
    	$members = 0;
    	foreach ($this->getMembers() as $m) {
    		if ($m->getName() || $m->getInstrument() || $m->getBirthday()) {
    			$members += 1;
    		} else {
    			$this->removeMember($m);
    		}
    	}
    	
    	if ($members < 2) {
    		$context->addViolationAt('members', '**Please enter at least 2 members.');
    	} elseif ($members > 4) {
    		$context->addViolationAt('members', '**A maximum of 4 band members are allowed.');
    	}
    	
    	$birthday = $this->getRepBirthday();
    	if ($birthday) {
    		$today = new \DateTime();
    		$diff = $birthday->diff($today);
    		$years = $diff->format('%y');
    		if ($years < 18 || $years > 35) {
    			$context->addViolationAt('repBirthday', 'Members should be between 18 and 35 years old from date of registration.');
    		}
    	}
    	
    	if (!empty($this->file)) {
    		if ($this->checkExtension($this->file->guessExtension()) === false) {
    			$context->addViolationAt("file", "Invalid video format uploaded.");
    		} else {
    			// this updates the file incase no other fields are updated.
    			$this->setUpdatedAt(new \DateTime('now'));
    		}
    	}
    }
    
    private function checkExtension($extension) {
    	
    	$allowedExtensions = array(
    		"mp4", "avi", "flv", "mov", "qt", "mkv", "vob", "wmv", "mpg", "mpeg", 
    		"m2v", "3gp", "3g2", "mxf", "ogv", "ogg", "webm", "rm", "rmvb", "m4v",
    		"asf"
    	);
    	return in_array($extension, $allowedExtensions);
    }  
}