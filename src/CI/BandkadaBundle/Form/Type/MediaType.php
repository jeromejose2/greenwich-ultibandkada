<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('file', 'file', array(
			'label' => 'Image',
			'attr' => array(
				'class' => 'file-input',
				'include_errors' => true
			)
		))
		->add('youtubeVideoEmbedCode', 'textarea', array(
			'required' => false,
			'attr' => array(
				'rows' => 4,
				'include_errors' => true
			)
		))
		->add('changeImage', 'hidden', array(
				'attr' => array('class' => 'change-image'),
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Media'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_media';
	}
}