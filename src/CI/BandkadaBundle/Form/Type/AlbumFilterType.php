<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AlbumFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'required' => false
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Event Date From',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Event Date To',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_albumfilter';
	}
}