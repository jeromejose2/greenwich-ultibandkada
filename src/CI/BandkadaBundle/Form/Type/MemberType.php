<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class MemberType extends AbstractType
{
	private $type;
	
	public function __construct($type = null)
	{
		$this->type = $type;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name', 'text', array(
			'attr' => array(
				'class' => 'inputTxt short'
			)
		))
		->add('instrument', 'text', array(
			'attr' => array(
				'class' => 'inputTxt short'
			)
		));
		
		if ($this->type === null) {
			$builder->add('birthday', 'date', array(
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'datepicker' => true,
					'class' => 'inputTxt short datepicker'
				)
			));
		} else {
			$builder->add('birthday', 'date', array(
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'attr'	   => array(
					'widget_col' => 5,
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			));
		}
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Member'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_member';
	}
}