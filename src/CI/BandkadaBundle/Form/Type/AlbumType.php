<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AlbumType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name')
		->add('eventDate', 'date', array(
			'label'    => 'Event Date',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => true,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('description', 'textarea', array(
			'attr' => array(
				'rows' => 5
			)
		))
		->add('media', 'collection', array(
			'type' => new MediaType(),
			'allow_add'=> true,
			'allow_delete'=> true,
			'by_reference' => false,
			'error_bubbling' => false
		))
		->add('file', 'file', array(
			'label' => 'Album Cover',
			'required' => true,
			'attr' => array(
				'class' => 'file-input'
			)
		))
		->add('changeImage', 'hidden', array(
			'attr' => array('class' => 'change-image'),
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Album'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_album';
	}
}