<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use CI\BandkadaBundle\Entity\Band;
use Symfony\Component\Validator\Constraints\NotBlank;

class BandNewType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('recaptcha', 'formextra_recaptcha', array(
			'widget_options' => array(
				'theme' => 'red'
			)
		))
		->add('repName', 'text', array(
			'attr' => array(
 				'class' => 'inputTxt'
			)
		))
		->add('mobileNumber', 'text', array(
			'attr' => array(
				'class' => 'inputTxt'
			)
		))
		->add('repInstrument', 'text', array(
			'attr' => array(
				'class' => 'inputTxt'
			)
		))
		->add('repBirthday', 'date', array(
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'datepicker' => true,
				'class' => 'inputTxt datepicker'
			)
		))
		->add('email', 'email', array(
			'attr' => array(
				'class' => 'inputTxt'
			)
		))
		->add('bandName', 'text', array(
			'attr' => array(
				'class' => 'inputTxt'
			)
		))
		->add('bandYoutubeUrl', 'url', array(
			'required' => false,
			'attr' => array(
				'class' => 'inputTxt spc_width'
			)
		))
		->add('genre', 'choice', array(
			'label' => false,
			'multiple' => true,
			'expanded' => true,
			'attr' => array(
				'inline' => true,
				'class' => 'lblopt'
			),
			'choices' => array(
				Band::GENRE_ROCK => Band::GENRE_ROCK,
				Band::GENRE_POP => Band::GENRE_POP,
				Band::GENRE_HIPHOP => Band::GENRE_HIPHOP,
				Band::GENRE_EDM => Band::GENRE_EDM
			)
		))
		->add('school', 'text', array(
			'required' => false,
			'attr' => array(
				'class' => 'inputTxt spc_width'
			)
		))
		->add('acknowledge', 'checkbox')
		->add('file', 'file', array(
			'attr' => array(
				'class' => 'custom-upload'
			)
		))
		->add('members', 'collection', array(
			'type' => new MemberType(),
			'allow_add'=> true,
			'allow_delete'=> true,
			'by_reference' => false,
			'error_bubbling' => false
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Band'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_band';
	}
}