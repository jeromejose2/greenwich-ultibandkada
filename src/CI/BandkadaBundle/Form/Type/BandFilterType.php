<?php

namespace CI\BandkadaBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use CI\BandkadaBundle\Entity\Band;

class BandFilterType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('repName', 'text', array(
			'label' => 'Representative Name',
			'required' => false,
		))
 		->add('band', 'entity', array(
			'class' => 'CIBandkadaBundle:Band',
			'property' => 'bandName',
 			'label' => 'Band Name',
 			'empty_value' => 'Choose a band',
			'required' => false,
			'attr' => array('class' => 'select2'),
			'query_builder' => function($er) {
 				return $er->findAllQb();
 			}
		))
		->add('email', 'text', array(
			'label' => 'Email',
			'required' => false,
		))
		->add('genre', 'choice', array(
			'label' => 'Genres',
			'required' => false,
			'multiple' => true,
			'attr'	   => array('class' => 'select2'),
			'choices' => array(
				Band::GENRE_ROCK => Band::GENRE_ROCK,
				Band::GENRE_POP => Band::GENRE_POP,
				Band::GENRE_HIPHOP => Band::GENRE_HIPHOP,
				Band::GENRE_EDM => Band::GENRE_EDM
			)
		))
		->add('dateFrom', 'date', array(
			'label'    => 'Registration Date From',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('dateTo', 'date', array(
			'label'    => 'Registration Date To',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'required' => false,
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('search', 'submit', array(
			'attr' => array(
				'class' => 'btn btn-outline submit-button',
				'data-loading-text' => "Searching..."
			)
		))
		;
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_bandfilter';
	}
}