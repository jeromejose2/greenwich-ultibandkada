<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use CI\BandkadaBundle\Entity\Band;

class BandType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('repName', 'text', array(
			'label' => 'Name'
		))
		->add('mobileNumber', 'text', array(
			'label' => 'Mobile #'
		))
		->add('repInstrument', 'text', array(
			'label' => 'Instrument'
		))
		->add('repBirthday', 'date', array(
			'label' => 'Birthday',
			'widget'   => 'single_text',
			'format'   => 'MM/dd/y',
			'attr'	   => array(
				'widget_col' => 5,
				'datepicker' => true,
				'input_group' => array('append' => 'calendar')
			)
		))
		->add('email', 'email')
		->add('bandName', 'text', array(
			'label' => 'Name'
		))
		->add('bandYoutubeUrl', 'url', array(
			'required' => false,
			'label' => 'Youtube Channel'
		))
		->add('bandVideoLink', 'text', array(
			'required' => false,
			'label' => 'Video Link'
		))
		->add('genre', 'choice', array(
			'label' => 'Genres',
			'multiple' => true,
			'expanded' => true,
			'attr' => array('inline' => true),
			'choices' => array(
				Band::GENRE_ROCK => Band::GENRE_ROCK,
				Band::GENRE_POP => Band::GENRE_POP,
				Band::GENRE_HIPHOP => Band::GENRE_HIPHOP,
				Band::GENRE_EDM => Band::GENRE_EDM
			)
		))
		->add('school', 'text', array(
			'required' => false
		))
		->add('members', 'collection', array(
			'type' => new MemberType('admin'),
			'allow_add'=> true,
			'allow_delete'=> true,
			'by_reference' => false,
			'error_bubbling' => false
		))
		
		->add('file', 'file', array(
			'label' => 'Video',
			'required' => false,
			'attr' => array(
				'class' => 'custom-upload',
				'help_text' => 'Replaces old video attached to this band.'
			)
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Band'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_band';
	}
}