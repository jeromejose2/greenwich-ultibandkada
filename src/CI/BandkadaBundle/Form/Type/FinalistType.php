<?php

namespace CI\BandkadaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FinalistType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('name')
		->add('shortWriteUp', 'textarea', array(
			'label' => 'Short Write-up',
			'attr' => array(
				'rows' => 4
			)
		))
		->add('writeUp', 'textarea', array(
			'label' => 'Write-up',
			'attr' => array(
				'rows' => 4
			)
		))
		->add('youtubeVideoEmbedCode', 'textarea', array(
			'label' => 'Youtube Video Embed Code',
			'required' => false,
			'attr' => array(
				'rows' => 4
			)
		))
		->add('teaserFile', 'file', array(
			'label' => 'Teaser Image',
			'attr' => array(
				'class' => 'file-input'
			)
		))
		->add('imageFile', 'file', array(
			'label' => 'Image',
			'attr' => array(
				'class' => 'file-input'
			)
		))
		->add('teaserChangeImage', 'hidden', array(
			'attr' => array('class' => 'change-image'),
		))
		->add('imageChangeImage', 'hidden', array(
			'attr' => array('class' => 'change-image'),
		))
		->add('save', 'submit', array(
			'label' => 'Save',
			'attr' => array(
				'class' => 'btn btn-primary submit-button',
				'data-loading-text' => "Saving..."
			)
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'CI\BandkadaBundle\Entity\Finalist'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'ci_bandkadabundle_finalist';
	}
}