<?php

namespace CI\BandkadaBundle\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;

use CI\BandkadaBundle\Model\BandModel as Model;

class DefaultController extends Controller
{
    /**
     * @Route("/terms", name="get_terms")
     * Ajax call for terms popup in band registration page.
     */
    public function termsAction()
    {
    	return $this->render('CIBandkadaBundle:Default:terms.html.twig');
    }
    
    /**
     * @Route("/success", name="get_success")
     * Ajax call for success popup in band registration page.
     */
    public function successAction()
    {
    	return $this->render('CIBandkadaBundle:Default:success.html.twig');
    }
    
    /**
     * @Route("/{id}/finalist", requirements={"id"="\d+"}, name="get_finalist")
     * Ajax call to get finalist info.
     */
    public function getFinalistAction($id)
    {
    	$entity = $this->get('ci.finalist.model')->findExistingEntity($id);
    	
    	return $this->render('CIBandkadaBundle:Default:finalist.inner.html.twig', array(
    		'finalist' => $entity
    	));
    }
    
    /**
     * Creates a new Band entity.
     *
     * @Route("/submit", name="band_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
    	$model = $this->get('ci.band.model');
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			
   				// save the registration
    			try {
    				$model->saveEntity($form, $entity);
    				$mailer = $this->get('mailer');
    				$message = $mailer->createMessage()
    					->setSubject('Confirming your submission to Greenwich #UltimateBandkada')
    					->setFrom('no-reply@ultimatebandkada.com')
    					->setTo($entity->getEmail())
    					->setBody(
    						$this->renderView(
    							'CIBandkadaBundle:Default:mail.html.twig',
    							array('name' => $entity->getRepName())
    					),
    					'text/html'
    				);
    				
    				$mailer->send($message);
    				return $this->redirect($this->generateUrl('band_new', array('success' => true)));
    			} catch (\Exception $e) {
    				//return to the form
    			}
    		}
    	}
    	
    	return $this->render('CIBandkadaBundle:Default:index.html.twig', array(
    		'entity' => $entity,
    		'form'   => $form->createView(),
    		'success' => false,
    		'isSubmitted' => true
    	));
    }
    
    /**
     * @Route("/", name="index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
    	$albumModel = $this->get('ci.album.model');
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$albumModel->getGallery(1),
    		1,
    		8
    	);

		$finalists = $this->get('ci.finalist.model')->getFinalists();

    	$albumId = $request->get('albumId');
    	$mediaId = $request->get('mediaId');
    	
    	try {
    		if (!empty($albumId)) {
    			$album = $albumModel->findExistingEntity($albumId);
    			$shared = array(
    				'albumId' => $albumId,
    				'album' => $album
    			);
    		
    			if (!empty($mediaId)) {
    				$media = $albumModel->findExistingMedia($mediaId);
    				$shared['mediaId'] = $mediaId;
    				$shared['media'] = $media;
    			}
    		} else {
    			$shared = null;
    		}
    	} catch (\Exception $e) {}
    
    	return $this->render('CIBandkadaBundle:Default:index.html.twig', array(
    		'shared' => isset($shared) ? $shared : null,
    		'finalists' => $finalists,
    		'pagination' => $pagination
    	));
    }
    
    /**
     * @Route("/{page}/{order}/gallery", name="gallery_album_list")
     * @Method("GET")
     */
    public function getGalleryAlbumListAction(Request $request, $page, $order)
    {
    	$albumModel = $this->get('ci.album.model');
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$albumModel->getGallery($order),
    		$page,
    		8
    	);
    
    	return new JsonResponse($this->render('CIBandkadaBundle:Default:galleryAlbumList.html.twig', array(
    		'pagination' => $pagination,
    		'order' => $order
    	))->getContent(), 200);
    }
    
    /**
     * @Route("/{id}/album", name="album_media_list")
     * @Method("GET")
     */
    public function getAlbumMediaListAction(Request $request, $id)
    {
    	$albumModel = $this->get('ci.album.model');
    	$album = $albumModel->findExistingEntity($id);
    	return new JsonResponse($this->render('CIBandkadaBundle:Default:albumMediaList.html.twig', array(
    		'album' => $album
    	))->getContent(), 200);
    }
    
    private function isValidVideoDuration($filepath)
    {
    	$ffprobe = $this->get('dubture_ffmpeg.ffprobe');
    	 
    	$duration = $ffprobe
	    	->format($filepath)
	    	->get('duration');
    	
    	return $duration <= 60 ? true : false;
    }
}
