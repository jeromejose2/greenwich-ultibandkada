<?php

namespace CI\BandkadaBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use CI\BandkadaBundle\Model\BandModel as Model;
use CI\BandkadaBundle\Entity\Band;

/**
 * Band controller.
 *
 * @Route("/band")
 */
class BandController extends BaseController
{
	const LINK_SHOW = 'band_show';
	
	public function getModel()
	{
		return $this->get('ci.band.model');
	}
	
    /**
     * Lists all Bands.
     *
     * @Route("/", name="band")
     * @Method("GET")
     */
    public function indexAction(Request $request) 
    {
    	$model = $this->getModel();
    	$form = $model->getFilterFormType();
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			$params = $form->getData();
    			$qb = $model->getIndex($params);
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
    		}
    	}
    	
    	if (!isset($qb)) {
    		$qb = $model->getIndex();
    	}
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$qb,
    		$this->get('request')->query->get('page', 1),
    		$this->container->getParameter('pagination_limit_per_page')
    	);
    	
    	return $this->render('CIBandkadaBundle:Band:index.html.twig', array(
    		'pagination' => isset($pagination) ? $pagination : null,
    		'search_form' => $form->createView()
    	));
    }

    /**
     * Finds and displays a Band entity.
     *
     * @Route("/{id}/show", requirements={"id"="\d+"}, name="band_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	return $this->render('CIBandkadaBundle:Band:show.html.twig', array('entity' => $entity));
    }

    /**
     * Displays a form to edit an existing Band entity.
     *
     * @Route("/{id}/edit", requirements={"id"="\d+"}, name="band_edit")
     * @Method("GET")
     */
    public function editAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isEditable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$editForm = $model->getFormType($entity);
    	
    	
    	return $this->render('CIBandkadaBundle:Band:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }

    /**
     * Edits an existing Band entity.
     *
     * @Route("/{id}", requirements={"id"="\d+"}, name="band_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	try {
    		$model->isEditable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    	
    	$editForm = $model->getFormType($entity);
    	
    	if ($editForm->handleRequest($request)->isSubmitted()) {
    		if ($editForm->isValid()) {
    			try {
    				$model->saveEntity($editForm, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_UPDATE));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return $this->render('CIBandkadaBundle:Band:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }

    /**
     * Deletes a Band entity.
     *
     * @Route("/{id}/delete", requirements={"id"="\d+"}, name="band_confirm_delete")
     * @Template("CIBandkadaBundle:Misc:delete.html.twig")
     */
    public function confirmDeleteAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$deleteForm = $this->createDeleteForm($id);
    	
    	try {
    		$model->isDeletable($entity);
    	} catch (\Exception $e) {
    		$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    		return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    	}
    		
    	return array(
    		'entity' => $entity,
    		'delete_form' => $deleteForm->createView(),
    		'params' => $model->getDeleteParams($entity)
    	);
    }
    
    /**
     * @Route("/{id}", requirements={"id"="\d+"}, name="band_delete")
     * @Method("DELETE")
     * @Template("CIBandkadaBundle:Misc:delete.html.twig")
     */
    public function deleteAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$form = $this->createDeleteForm($id);
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			try {
    				$model->deleteEntity($id);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages(Model::ACTION_DELETE));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again later.');
    		}
    	}
    	
    	return $this->redirect($this->generateUrl('project'));
    }
    
    /**
     * @Route("/revision/{id}", requirements={"id"="\d+"}, name="band_revision")
     * @Template("CIBandkadaBundle:Log:index.html.twig")
     */
    public function revisionAction($id)
    {
    	$model = $this->getModel();
    	$revision = $model->getRevision($id);
    	return $this->entityRevision($revision['route'], $revision['name'], $revision['class']);
    }
    
    /**
     * @Route("/revision/{parentId}/{id}/log", requirements={"id"="\d+", "parentId"="\d+"}, name="band_log")
     * @Template("CIBandkadaBundle:Log:show.html.twig")
     */
    public function logAction($parentId, $id)
    {
    	$model = $this->getModel();
    	$log = $model->getLog();
    	return $this->entityLog($parentId, $log['route'], $id, $log['name'], $log['classes']);
    }
    
    /**
	 * Download file.
	 *
	 * @Route("/{id}/download", name="band_video_download")
	 */
	public function downloadAction($id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$file = $entity->getAbsolutePath();
		 
		if (file_exists($file)) {
			$fileObj = new File($file);
			$mimeType = $fileObj->getMimeType();
			
			$response = new Response();
			
			$d = $response->headers->makeDisposition(
				ResponseHeaderBag::DISPOSITION_ATTACHMENT,
				$fileObj->getFilename()
			);
			$response->headers->set('Content-Disposition', $d);
			$response->headers->set('Content-type', $mimeType);
			$response->headers->set('Content-length', filesize($file));
			$response->sendHeaders();
			$response->setContent(readfile($file));
			return $response;
		} else {
			throw $this->createNotFoundException('File not found');
		}
	}
	
	/**
	 * @Route("/export/xls", name="band_list_export_xls")
	 */
	public function bandListExportXLSAction(Request $request)
	{
		$model = $this->getModel();
		$objWriter = $model->exportAll();
		
		$response = new Response();
		$response->headers->set('Content-Type', 'application/vnd.ms-excel');
		
		$filename = 'Band_List_As_of_' . date('M-d-Y');
		
		$response->headers->set('Content-Disposition', 'attachment;filename='. $filename . '.xls');
		$response->headers->set('Cache-Control', 'ax-age=0');
		$response->sendHeaders();
		
		$objWriter->save('php://output');
		exit();
	}
}