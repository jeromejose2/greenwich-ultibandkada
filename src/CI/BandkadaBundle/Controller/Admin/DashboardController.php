<?php

namespace CI\BandkadaBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CI\BandkadaBundle\Model\DashboardModel as Model;

/**
 * Dashboard controller
 *
 */
class DashboardController extends Controller
{
	public function getModel()
	{
		return $this->get('ci.dashboard.model');
	}
	
    /**
     * Dashboard
     *
     * @Route("/", name="dashboard")
     */
    public function indexAction()
    {
    	$model = $this->getModel();
    	return $this->render('CIBandkadaBundle:Dashboard:index.html.twig', $model->getIndex());
    }
}