<?php

namespace CI\BandkadaBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Album controller.
 *
 * @Route("/album")
 */
class AlbumController extends BaseController
{
	const LINK_SHOW = 'album_show';
	
	public function getModel()
	{
		return $this->get('ci.album.model');
	}
	
    /**
     * Lists all Album.
     *
     * @Route("/", name="album")
     * @Method("GET")
     */
    public function indexAction(Request $request) 
    {
    	$model = $this->getModel();
    	$form = $model->getFilterFormType();
    	
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			$params = $form->getData();
    			$qb = $model->getIndex($params);
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please try again.');
    		}
    	}
    	
    	if (!isset($qb)) {
    		$qb = $model->getIndex();
    	}
    	
    	$paginator = $this->get('knp_paginator');
    	$pagination = $paginator->paginate(
    		$qb,
    		$this->get('request')->query->get('page', 1),
    		$this->container->getParameter('pagination_limit_per_page')
    	);
    	
    	return $this->render('CIBandkadaBundle:Album:index.html.twig', array(
    		'pagination' => isset($pagination) ? $pagination : null,
    		'search_form' => $form->createView()
    	));
    }
    
    /**
     * Creates a new Album entity.
     *
     * @Route("/", name="album_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    
    	if ($form->handleRequest($request)->isSubmitted()) {
    		if ($form->isValid()) {
    			try {
    				$model->saveEntity($form, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages('create'));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    
    	return $this->render('CIBandkadaBundle:Album:new.html.twig', array(
    		'entity'    => $entity,
    		'form' => $form->createView()
    	));
    }
    
    /**
     * Displays a form to create a new Album entity.
     *
     * @Route("/new", name="album_new")
     * @Method("GET")
     */
    public function newAction()
    {
    	$model = $this->getModel();
    	$entity = $model->getNewEntity();
    	$form = $model->getFormType($entity);
    
    	return $this->render('CIBandkadaBundle:Album:new.html.twig', array(
    		'entity'    => $entity,
    		'form' => $form->createView()
    	));
    }

    /**
     * Finds and displays a Album entity.
     *
     * @Route("/{id}/show", requirements={"id"="\d+"}, name="album_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	
    	return $this->render('CIBandkadaBundle:Album:show.html.twig', array('entity' => $entity));
    }

    /**
     * Displays a form to edit an existing Album entity.
     *
     * @Route("/{id}/edit", requirements={"id"="\d+"}, name="album_edit")
     * @Method("GET")
     */
    public function editAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$editForm = $model->getFormType($entity);
    	
    	
    	return $this->render('CIBandkadaBundle:Album:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }

    /**
     * Edits an existing Band entity.
     *
     * @Route("/{id}", requirements={"id"="\d+"}, name="album_update")
     * @Method("PUT")
     */
    public function updateAction(Request $request, $id)
    {
    	$model = $this->getModel();
    	$entity = $model->findExistingEntity($id);
    	$model->storeOriginalItems($entity);
    	$editForm = $model->getFormType($entity);
    	
    	if ($editForm->handleRequest($request)->isSubmitted()) {
    		if ($editForm->isValid()) {
    			try {
    				$model->saveEntity($editForm, $entity);
    				$this->get('session')->getFlashBag()->add('success', $model->getMessages('update'));
    				return $this->redirect($this->generateUrl(self::LINK_SHOW, array('id' => $entity->getId())));
    			} catch (\Exception $e) {
    				$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
    			}
    		} else {
    			$this->get('session')->getFlashBag()->add('danger', 'Please fill in the correct values.');
    		}
    	}
    	
    	return $this->render('CIBandkadaBundle:Album:edit.html.twig', array(
    		'entity'    => $entity,
    		'edit_form' => $editForm->createView()
    	));
    }
    
    /**
	 * Creates a delete form.
	 *
	 * @Route("/confirm-delete/{id}", requirements={"id"="\d+"}, name="album_confirm_delete")
	 * @Method("GET")
	 * @Template("CIBandkadaBundle:Misc:delete.html.twig")
	 */
	public function confirmDeleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$entity = $model->findExistingEntity($id);
		$deleteForm = $this->createDeleteForm($id);
			
		return array(
			'entity' => $entity,
			'delete_form' => $deleteForm->createView(),
			'params' => $model->getDeleteParams($entity)
		);
	}
	
	/**
	 * Deletes an Album entity.
	 *
	 * @Route("/{id}", requirements={"id"="\d+"}, name="album_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, $id)
	{
		$model = $this->getModel();
		$form = $this->createDeleteForm($id);
		
		if ($form->handleRequest($request)->isSubmitted()) {
			if ($form->isValid()) {
				try {
					$model->deleteEntity($id);
					$this->get('session')->getFlashBag()->add('success', $model->getMessages('delete'));
				} catch (\Exception $e) {
					$this->get('session')->getFlashBag()->add('danger', $e->getMessage());
				}
			} else {
				$this->get('session')->getFlashBag()->add('danger', 'Something went wrong. Please try again.');
			}
		}
		
		return $this->redirect($this->generateUrl('album'));
	}
}