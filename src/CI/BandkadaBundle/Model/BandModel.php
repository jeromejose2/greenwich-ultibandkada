<?php

namespace CI\BandkadaBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PHPExcel;
use PHPExcel_IOFactory;

use CI\BandkadaBundle\Entity\Band as EntityType;
use CI\BandkadaBundle\Entity\Member as EntityItemType;
use CI\BandkadaBundle\Form\Type\BandFilterType as FilterFormType;
use CI\BandkadaBundle\Form\Type\BandNewType;
use CI\BandkadaBundle\Form\Type\BandType;
use CI\BandkadaBundle\Entity\Member;

class BandModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	const ACTION_STATUS_CHANGE = 'statusChange';
	
	public function getNewEntity()
	{
		$entity = new EntityType();
		$entity->addMember(new Member());
		$entity->addMember(new Member());
		$entity->addMember(new Member());
		$entity->addMember(new Member());
		return $entity;
	}
	
	public function getFormType($entity = null)
	{
		if (isset($entity)) {
			if ($entity->getId() === null) {
				$form = new BandNewType();
			}
		}
		
		if (empty($form)) {
			$form = new BandType();
		}
		
		return $this->getFormFactory()->create($form, $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FilterFormType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: return 'Band created successfully.';
			case self::ACTION_UPDATE: return 'Band updated successfully.';
			case self::ACTION_DELETE: return 'Band has been deleted.';
			default: throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(EntityType $entity)
	{
		
	}
	
	public function isEditable(EntityType $entity)
	{
		
	}
	
	public function getIndex($params = null)
	{
		return $this->getRepository()->findAll($params);
	}
	
	public function saveEntity(Form $form, $entity, $isAjax = false)
	{
		$em = $this->getEM();
		
		if ($entity->getId()) {
			$entity->setUpdatedAt(new \DateTime('now'));
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
    		'path' => 'band_delete',
    		'return_path' => 'band_show',
    		'name' => '[Band] ' . $entity->getBandName()
    	);
	}
	
	public function getRevision($id)
	{
		$classes = array(
			'id' => $id,
			'class' => 'Band'
		);
	
		$options = array(
			'route' => 'band',
			'name' => 'Band',
			'class' => $classes,
		);
	
		return $options;
	}
	
	public function getLog()
	{
		return array(
			'route' => 'band',
			'name' => 'Band',
			'classes' => array(
				'CI\BandkadaBundle\Entity\Band'
			)
		);
	}
	
	public function exportAll()
	{
		$objPHPExcel = new PHPExcel();
		
		$fontStyle = array('bold' => true);
		
		$styleArray = array(
			'font' => $fontStyle,
			'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
		);
		
		$objPHPExcel->getProperties()->setCreator("Greenwich")->setTitle("Bandkada Band List");
		
		$xls = $objPHPExcel->setActiveSheetIndex(0);
		
		$headers = array(
			'Band Rep Name', 'Band Rep Mobile #', 'Band Rep Instrument', 'Band Rep Birthday', 'Email',
			'Band Name', 'Youtube Channel', 'Genres', 'School',
			'Member 1 Name', 'Member 1 Instrument', 'Member 1 Birthday',
			'Member 2 Name', 'Member 2 Instrument', 'Member 2 Birthday',
			'Member 3 Name', 'Member 3 Instrument', 'Member 3 Birthday',
			'Member 4 Name', 'Member 4 Instrument', 'Member 4 Birthday'
		);
		
		$letter = "A";
		foreach ($headers as $header) {
			$xls->setCellValue($letter . '1', $header);
			$xls->getColumnDimension($letter)->setAutoSize(true);
			$letter++;
		}
		$xls->getStyle("A1:" . chr(ord($letter) - 1) ."1")->applyFromArray($styleArray);
		
		$bands = $this->getRepository()->findAll()->getResult();
		
		$counter = 2;
		foreach($bands as $band) {
			$xls->setCellValue('A' . $counter, $band->getRepName());
			$xls->setCellValue('B' . $counter, $band->getMobileNumber());
			$xls->setCellValue('C' . $counter, $band->getRepInstrument());
			$xls->setCellValue('D' . $counter, $band->getRepBirthday()->format('M. d, Y'));
			$xls->setCellValue('E' . $counter, $band->getEmail());
			$xls->setCellValue('F' . $counter, $band->getBandName());
			$xls->setCellValue('G' . $counter, $band->getBandYoutubeUrl());
			$xls->setCellValue('H' . $counter, implode(", ", $band->getGenre()));
			$xls->setCellValue('I' . $counter, $band->getSchool());
			
			$letter = 'J';
			foreach ($band->getMembers() as $member) {
				$xls->setCellValue($letter . $counter, $member->getName()); $letter++;
				$xls->setCellValue($letter . $counter, $member->getInstrument()); $letter++;
				$xls->setCellValue($letter . $counter, $member->getBirthday()->format('M. d, Y')); $letter++;
			}
			$counter++;
		}
		
		$xls->setSelectedCell();
		$objPHPExcel->setActiveSheetIndex(0);
		
		return PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	}
}