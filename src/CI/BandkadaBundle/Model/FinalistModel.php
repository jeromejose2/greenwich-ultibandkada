<?php

namespace CI\BandkadaBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

use CI\BandkadaBundle\Form\Type\FinalistFilterType;
use CI\BandkadaBundle\Form\Type\FinalistType;
use CI\BandkadaBundle\Entity\Finalist;

class FinalistModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	private $originalItems;
	
	public function getNewEntity()
	{
		return new Finalist();
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new FinalistType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new FinalistFilterType(), null, array('method' => 'GET'));
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: 
				return 'Finalist created successfully.';
			case self::ACTION_UPDATE: 
				return 'Finalist updated successfully.';
			case self::ACTION_DELETE: 
				return 'Finalist has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Finalist $entity)
	{
		
	}
	
	public function isEditable(Finalist $entity)
	{
		
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'finalist_delete',
			'return_path' => 'finalist_show',
			'name' => '[Finalist] ' . $entity->getName()
		);
	}
	
	public function getRevision($id)
	{
		
	}
	
	public function getLog()
	{
		
	}
	
	/**
	 * Used in site for getting finalists.
	 */
	public function getFinalists()
	{
		return $this->getRepository()->getFinalists();
	}
}