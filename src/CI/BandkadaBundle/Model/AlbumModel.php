<?php

namespace CI\BandkadaBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;

use CI\BandkadaBundle\Form\Type\AlbumFilterType;
use CI\BandkadaBundle\Form\Type\AlbumType;
use CI\BandkadaBundle\Entity\Album;
use CI\BandkadaBundle\Entity\Media;

class AlbumModel extends BaseEntityModel
{
	const ACTION_CREATE = 'create';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';
	
	private $originalItems;
	
	public function getNewEntity()
	{
		$entity = new Album();
		$entity->addMedium(new Media());
		
		return $entity;
	}
	
	public function getFormType($entity = null)
	{
		return $this->getFormFactory()->create(new AlbumType(), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}
	
	public function getFilterFormType()
	{
		return $this->getFormFactory()->create(new AlbumFilterType(), null, array('method' => 'GET'));
	}
	
	public function findExistingMedia($id)
	{
		return $this->getEM()->getRepository('CIBandkadaBundle:Media')->find($id);
	}
	
	public function getMessages($action)
	{
		switch($action) {
			case self::ACTION_CREATE: 
				return 'Album created successfully.';
			case self::ACTION_UPDATE: 
				return 'Album updated successfully.';
			case self::ACTION_DELETE: 
				return 'Album has been deleted.';
			default: 
				throw new \Exception('Invalid action parameter.');
		}
	}
	
	public function isDeletable(Album $entity)
	{
		
	}
	
	public function isEditable(Album $entity)
	{
		
	}
	
	public function storeOriginalItems($entity)
	{
		$this->originalItems = new ArrayCollection();
	
		foreach ($entity->getMedia() as $m) {
			$this->originalItems->add($m);
		}
	}
	
	public function saveEntity(Form $form, $entity)
	{
		$em = $this->getEM();
		
		if ($entity->getId()) {
			foreach ($this->originalItems as $m) {
				if (false === $entity->getMedia()->contains($m)) {
					$em->remove($m);
				}
			}
				
			$entity->setUpdatedAt(new \DateTime());
		}
		
		$em->persist($entity);
		$em->flush();
	}
	
	public function getDeleteParams($entity)
	{
		return array(
			'path' => 'album_delete',
			'return_path' => 'album_show',
			'name' => '[Album] ' . $entity->getName()
		);
	}
	
	public function getRevision($id)
	{
		
	}
	
	public function getLog()
	{
		
	}
	
	public function getGallery($order = 1)
	{
		if ($order == 2) {
			$order = "ASC";
		} else {
			$order = "DESC";
		}
		return $this->getRepository()->getGallery($order);
	}
}