<?php

namespace CI\CoreBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use CI\CoreBundle\Form\EventListener\AddRoleFieldSubscriber;

class CustomProfileFormType extends AbstractType
{
	protected $securityContext;
	
	public function __construct(SecurityContext $securityContext)
	{
		$this->securityContext = $securityContext;
	}
	
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$subscriber = new AddRoleFieldSubscriber($builder->getFormFactory(), $this->securityContext);
		$builder->addEventSubscriber($subscriber);
		$builder
			->add('firstName', 'text', array(
				'label' => 'First Name'
			))
			->add('lastName', 'text', array(
				'label' => 'Last Name',
				'required' => false
			))
			->add('username')
			->add('email', 'email')
			->add('gender', 'choice', array(
				'expanded' => true,
				'choices' => array(
					'M' => 'Male', 
					'F' => 'Female'
				)
			))
			->add('birthDate', 'date', array(
				'label'    => 'Birthday',
				'widget'   => 'single_text',
				'format'   => 'MM/dd/y',
				'required' => false,
				'attr'	   => array(
					'datepicker' => true,
					'input_group' => array('append' => 'calendar')
				)
			))
			->add('contactNumber', 'text', array(
				'label' => 'Contact Number'
			))
		;
	}
	
	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'CI\CoreBundle\Entity\User'
		);
	}
	
	public function getName()
	{
		return 'ci_core_custom_user_edit_profileform';
	}
}