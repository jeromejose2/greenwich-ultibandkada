<?php
namespace CI\CoreBundle\Twig;

class StringUtilityExtension extends \Twig_Extension
{
	/**
	 * {@inheritDoc}
	 */
	public function getFunctions()
	{
		return array(
			new \Twig_SimpleFunction('ucwords', array($this, 'applyUCWords'))
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('ucwords', array($this, 'applyUCWords'))
		);
	}
	
	public function applyUCWords($str)
	{
		return ucwords($str);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getName()
	{
		return 'ci_string_utility';
	}
}