<?php

namespace CI\CoreBundle\Entity;

use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="user") 
 * @ORM\Entity(repositoryClass="CI\CoreBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="email", message="Sorry, this email address is already taken.")
 * @UniqueEntity(fields="username", message="Sorry, this username is already taken.")
 */
class User extends BaseUser
{
	const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
	const ROLE_ADMIN = 'ROLE_ADMIN';
	// add other roles here
	
	const ACCESS_DEACTIVATE = 'deactivate';
	const ACCESS_REACTIVATE = 'reactivate';
	
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string", length=100)
	 */
	private $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
	 */
	private $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gender", type="string", length=1)
	 */
	private $gender;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="birth_date", type="datetime", nullable=true)
	 */
	private $birthDate;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="contact_number", type="text", length=25, nullable=true)
	 */
	private $contactNumber;
	
	/**
     * @var datetime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="created_by", type="string", length=255)
     * @Gedmo\Blameable(on="create")
     */
    protected $createdBy;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     * @Gedmo\Blameable(on="update")
     */
    protected $updatedBy;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_sidebar_collapsed", type="boolean")
     */
    private $isSidebarCollapsed;
    
    /**
     * @ORM\OneToMany(targetEntity="UserLog", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userLogs;
    
	public function __construct()
	{
		parent::__construct();
		$this->userLogs = new \Doctrine\Common\Collections\ArrayCollection();
		$this->isSidebarCollapsed = false;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * Get firstName
	 *
	 * @return string
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * Set lastName
	 *
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * Get lastName
	 *
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * Set gender
	 *
	 * @param string $gender
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * Get gender
	 *
	 * @return string
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * Set birthDate
	 *
	 * @param date $birthDate
	 */
	public function setBirthDate($birthDate)
	{
		$this->birthDate = $birthDate;
	}

	/**
	 * Get birthDate
	 *
	 * @return date
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

    /**
     * Set contactNumber
     *
     * @param text $contactNumber
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;
    }

    /**
     * Get contactNumber
     *
     * @return text 
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }
    
	/**
     * Set created_at
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt = null)
    {
    	$this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated_at
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt = null)
    {   
    	$this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return User
     */
    public function setCreatedBy($createdBy)
    {
    	$this->createdBy = $createdBy;
    
    	return $this;
    }
    
    /**
     * Get createdBy
     *
     * @return string
     */
    public function getCreatedBy()
    {
    	return $this->createdBy;
    }
    
    /**
     * Set updatedBy
     *
     * @param string $updatedBy
     * @return User
     */
    public function setUpdatedBy($updatedBy)
    {
    	$this->updatedBy = $updatedBy;
    
    	return $this;
    }
    
    /**
     * Get updatedBy
     *
     * @return string
     */
    public function getUpdatedBy()
    {
    	return $this->updatedBy;
    }
    
    /**
     * Get userLogs
     * 
     * @return ArrayCollection
     */
    public function getUserLogs()
    {
    	return $this->userLogs;
    }

    /**
     * Add userLogs
     *
     * @param \CI\CoreBundle\Entity\UserLog $userLogs
     * @return User
     */
    public function addUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
    {
        $this->userLogs[] = $userLogs;
    
        return $this;
    }

    /**
     * Remove userLogs
     *
     * @param \CI\CoreBundle\Entity\UserLog $userLogs
     */
    public function removeUserLog(\CI\CoreBundle\Entity\UserLog $userLogs)
    {
        $this->userLogs->removeElement($userLogs);
    }
    
    /**
     * Set isSidebarCollapsed
     *
     * @param boolean $isSidebarCollapsed
     * @return User
     */
    public function setIsSidebarCollapsed($isSidebarCollapsed)
    {
    	$this->isSidebarCollapsed = $isSidebarCollapsed;
    
    	return $this;
    }
    
    /**
     * Get isSidebarCollapsed
     *
     * @return boolean
     */
    public function getIsSidebarCollapsed()
    {
    	return $this->isSidebarCollapsed;
    }
    
    
    public function getName()
    {
    	return $this->getFirstName() . ' ' . $this->getLastName();
    }
}