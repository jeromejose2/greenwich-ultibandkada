<?php 

namespace CI\CoreBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\CoreBundle\Form\Type\CustomProfileFormType;
use CI\CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserModel
{
	const REPOSITORY = 'CICoreBundle:User';
	const ACTION_UPDATE = 'update';
	const ACTION_DEACTIVATE = 'deactivate';
	const ACTION_REACTIVATE = 'reactivate';

	private $container;
	private $formFactory;
	private $em;
	private $securityContext;

	public function __construct(ContainerInterface $container, FormFactoryInterface $formFactory)
	{
		$this->container = $container;
		$this->formFactory = $formFactory;
		$this->em = $container->get('doctrine')->getManager();
		$this->securityContext = $container->get('security.context');
	}

	public function findExistingEntity($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->find($id);
	}

	public function getType(User $entity = null)
	{
		return $this->formFactory->create(new CustomProfileFormType($this->securityContext), $entity, $entity->getId() ? array('method' => 'PUT') : array());
	}

	public function getMessages($action)
	{
		switch ($action) {
			case self::ACTION_UPDATE: 
				return 'User has been updated successfully.';
			case self::ACTION_DEACTIVATE:
				return 'User has been deactivated.';
			case self::ACTION_REACTIVATE:
				return 'User has been reactivated.';
		}
	}

	public function getIndex()
	{
		return $this->em->getRepository(self::REPOSITORY)->findAll();
	}

	public function saveEntity(User $entity)
	{
		$this->em->persist($entity);
		$this->em->flush();
	}

	public function manageAccess($id, $access)
	{
		$entity = $this->findExistingEntity($id);
		$access == User::ACCESS_DEACTIVATE ? $entity->setEnabled(false) : $entity->setEnabled(true);
		$this->saveEntity($entity);
	}

	public function getLoginHistory($id)
	{
		return $this->em->getRepository(self::REPOSITORY)->findLoginHistory($id);
	}
	
	public function setSidebarCollapsed($request)
	{
		$sidebarCollapsed = $request->request->get('sidebarCollapsed') == 'expanded' ? false : true;
		$user = $this->securityContext->getToken()->getUser();
		
		$user->setIsSidebarCollapsed($sidebarCollapsed);
		
		$em = $this->em;
		$em->persist($user);
		$em->flush();
		
		return new JsonResponse($sidebarCollapsed, 200);
	}
}